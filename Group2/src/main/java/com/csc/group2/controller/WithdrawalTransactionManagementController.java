package com.csc.group2.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.csc.group2.dto.AuthenticationResult;
import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.WithdrawalTransaction;
import com.csc.group2.mvc.model.WithdrawalTransactionError;
import com.csc.group2.service.AppService;

@Controller
public class WithdrawalTransactionManagementController {

	@Autowired
	private AppService service;

	@RequestMapping(value = "/withdrawal-transaction")
	public String showWithdrawalTransactions(HttpServletRequest request,
			Model model, HttpSession session) {

		AuthenticationResult rs = service.checkAuthentication(session);

		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isSupervisor()) {
				return "redirect:/withdrawal-transaction-supervisor.html";
			}
		}

		String returnPage = "";

		String accountNumber = request.getParameter("accountNumber");
		if (accountNumber != null) {
			System.out.println(accountNumber);
			AccountInfo accountInfo = service
					.getAccountInfoByAccountNumber(accountNumber);
			model.addAttribute("account", accountInfo);
		}

		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		String toDate = df.format(date);

		List<WithdrawalTransaction> withdrawalTransactions = service
				.getWithdrawalTransactionsByTeller(rs.getEmployee());

		model.addAttribute("listWithdrawalTransaction", withdrawalTransactions);
		model.addAttribute("toDate", toDate);
		
		returnPage = "withdrawal-transaction";
		return returnPage;
	}

	@RequestMapping(value = "/withdrawal-transaction-supervisor")
	public String aprroveWithdrawalTransactions(Model model, HttpSession session) {
		
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isTeller()) {
				return "redirect:/withdrawal-transaction.html";
			}
		}
		Employee suppervisor = rs.getEmployee();

		List<WithdrawalTransaction> listPending = service.getWithdrawalTransactionsBySupervisor(suppervisor);
		List<WithdrawalTransaction> listApproved = service.getWithdrawalTransactionsByApprove(suppervisor.getId());
		
		// TODO must sort here
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		String toDate = df.format(date);
		model.addAttribute("listPending", listPending);
		model.addAttribute("listApproved", listApproved);
		model.addAttribute("toDate", toDate);
		return "withdrawal-transaction-supervisor";
	}

	@RequestMapping(value = "createWithdrawal", method = RequestMethod.POST)
	public String createWithdrawalTransaction(HttpServletRequest request, Model model) {

		AuthenticationResult rs = service.checkAuthentication(request
				.getSession());
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		}

		boolean isValidationOK = true;
		WithdrawalTransactionError withdrawalTransactionError = new WithdrawalTransactionError();

		int teller_Id = rs.getEmployee().getId();
		String accountNo = request.getParameter("acountNo");
//		String amountInWords = request.getParameter("amountInWords");
		String amountInfigues = request.getParameter("amountInfigues");
		String detail = request.getParameter("detail");

		//
		ValidateResult accountCheck = checkAccountNo(accountNo);
		if (!accountCheck.isGood()) {
			isValidationOK = false;
			withdrawalTransactionError.setNumber(accountCheck.getError());
		}

		ValidateResult detailCheck = checkDetail(detail);
		if (!detailCheck.isGood()) {
			isValidationOK = false;
			withdrawalTransactionError.setDetail(accountCheck.getError());
		}
		
		Date date = new Date();

		double amountDouble = 0;
		try {
			amountDouble = Double.parseDouble(amountInfigues);
		} catch (Exception e) {
			isValidationOK = false;
			withdrawalTransactionError.setNumber("Invalid Number");
			System.out.println(e.getMessage());
		}
		
		int supervisor_Id = rs.getEmployee().getTransactionGroup()
				.getSupervisor().getId();

		

		if (!isValidationOK) {
			model.addAttribute("error", withdrawalTransactionError);
			return "forward:/withdrawal-transaction";
		} else {
			withdrawalTransactionError = service.saveTransaction(detail, amountDouble, date, null,
					date, accountNo, teller_Id, supervisor_Id);
			
			if (withdrawalTransactionError.isHasError()){
				model.addAttribute("error", withdrawalTransactionError);
				return "forward:/withdrawal-transaction.html";
			}
		}

		// fine case;
		return "redirect:/withdrawal-transaction.html";
	}

	private ValidateResult checkDetail(String detail) {
		
		if (null == detail){
			return new ValidateResult("No detail", false);
		} else {
			if ("".endsWith(detail)){
				return new ValidateResult("No detail", false);
			}
		}
		
		return new ValidateResult("",true);
	}

	@RequestMapping(value = "approveWithdrawalTransaction")
	public String approveWithdrawalTransaction(
			@RequestParam("transactionId") int transactionId,
			HttpServletRequest request) {

		AuthenticationResult rs = service.checkAuthentication(request
				.getSession());
		if (!rs.isSupervisor()) {
			return rs.getRecommentAction();
		}

		WithdrawalTransaction withdrawalTransaction = service
				.getWithdrawalTransaction(transactionId);
		service.approveTransaction(withdrawalTransaction, rs.getEmployee()
				.getId());
		return "redirect:/withdrawal-transaction.html";
	}

	@RequestMapping(value = "rejectWithdrawalTransaction")
	public String cancelWithdrawalTransaction(@RequestParam("transactionId") int id,
			HttpServletRequest request) {

		AuthenticationResult rs = service.checkAuthentication(request
				.getSession());
		if (!rs.isSupervisor()) {
			return rs.getRecommentAction();
		}

		service.rejectTransaction(id, rs.getEmployee().getId());
		return "redirect:/withdrawal-transaction.html";
	}

	public ValidateResult checkAccountNo(String accountNo) {
	
		if (accountNo == null){
			return new ValidateResult("Invalid account number", false);
			
		} else {
			AccountInfo accountInfo = service.getAccountInfoByAccountNumber(accountNo);
			if (accountInfo == null){
				return new ValidateResult("Invalid account number", false);
			}
		}
		return new ValidateResult("", true);
	}

	class ValidateResult {
		
		
		public ValidateResult() {
			super();
			// TODO Auto-generated constructor stub
		}

		public ValidateResult(String error, boolean good) {
			super();
			this.error = error;
			this.good = good;
		}

		String error = "";
		boolean good = true;

		public String getError() {
			return error;
		}

		public void setError(String error) {
			this.error = error;
		}

		public boolean isGood() {
			return good;
		}

		public boolean isBad() {
			return !good;
		}

		public void setGood(boolean good) {
			this.good = good;
		}
	}
}
