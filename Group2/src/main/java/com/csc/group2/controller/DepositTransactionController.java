package com.csc.group2.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.csc.group2.dto.AuthenticationResult;
import com.csc.group2.dto.DepositTransactionDTO;
import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.BankBranch;
import com.csc.group2.entities.DepositTransaction;
import com.csc.group2.entities.Employee;
import com.csc.group2.service.AppService;

@Controller
public class DepositTransactionController {

	@Autowired
	private AppService service;
	
	//Controller Teller Begin 
	@RequestMapping(value = "/deposit-transaction")
	public ModelAndView showAccountManagement(HttpServletRequest request, HttpSession session) {
		ModelAndView mv = new ModelAndView("deposit-transaction");
		Employee teller = (Employee) session.getAttribute("employee");
		List<DepositTransactionDTO> listDepositTransaction = service
				.getAllDepositTransactionsByTeller(teller);
		Date date = new Date();
		SimpleDateFormat dt = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		mv.addObject("date", dt.format(date));
		mv.addObject("listDepositTransaction", listDepositTransaction);
		mv.addObject("depositTransactionInfo", new DepositTransactionDTO());
		String accountNumber = request.getParameter("accountNumber");
		if(accountNumber != null) {
			AccountInfo  accountInfo = service.getAccountInfoByAccountNumber(accountNumber);
			mv.addObject("account", accountInfo);
		}
		return mv;
	}

	@RequestMapping(value = "createDepositTransaction")
	public ModelAndView createDepositTransaction(
			@ModelAttribute("depositTransactionInfo") @Valid DepositTransactionDTO depositTransactionInfo,
			BindingResult result, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		Employee teller = (Employee) session.getAttribute("employee");
		try {
			if (result.hasErrors()) {
				List<DepositTransactionDTO> listDepositTransaction = service
						.getAllDepositTransactionsByTeller(teller);
				mv.addObject("listDepositTransaction", listDepositTransaction);
				mv.addObject("depositTransactionInfo", depositTransactionInfo);
				mv.setViewName("deposit-transaction");
			} else {
				AccountInfo accountInfo = service.getAccountInfoByAccountNumber(depositTransactionInfo.getAccount());
				if (accountInfo != null) {
					mv.setViewName("redirect:/deposit-transaction.html");
					Date date = new Date();
					BankBranch bankBranch = teller.getBankBranch();
					DepositTransaction depositTransaction = new DepositTransaction(
							depositTransactionInfo.getDepositorName(), depositTransactionInfo.getDepositorIdCard(), depositTransactionInfo.getDepositorAddress()
							, depositTransactionInfo.getDetail(), depositTransactionInfo.getAmount(), date, null, Constant.PENDING, accountInfo, bankBranch,
							teller, teller.getTransactionGroup().getSupervisor());
					service.addDepositTransaction(depositTransaction);
				} else {
					mv.addObject("messageError", "Don't find Account Info with Account Number");
					mv.setViewName("deposit-transaction");
					System.out.println("Don't find Account Info with Account Number");
				}
			}
		} catch (Exception e) {
			
		}

		return mv;
	}
	//Controller Teller End
	
	//Controller Supervisor Begin 
	@RequestMapping(value = "/supervisor-deposit-transaction")
	public ModelAndView showSuperVisorAccountManagement(HttpSession session) {
		ModelAndView mv = new ModelAndView("supervisor-deposit-transaction");
		Employee supervisor = (Employee) session.getAttribute("employee");
		List<DepositTransactionDTO> listDepositTransactionPending = service
				.getAllDepositTransactionsBySuperVisorPending(supervisor);
		List<DepositTransactionDTO> listDepositTransactionNotPending = service.getAllDepositTransactionDTOBySuperVisorNotPending(supervisor);
		Date date = new Date();
		SimpleDateFormat dt = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		mv.addObject("date", dt.format(date));
		mv.addObject("listDepositTransactionPending", listDepositTransactionPending);
		mv.addObject("listDepositTransactionNotPending", listDepositTransactionNotPending);
		return mv;
	}
	
	
	@RequestMapping(value = "/getDetailDepositTransaction", method = RequestMethod.POST)
	public @ResponseBody DepositTransactionDTO getDetailDepositTransaction(@RequestParam("transactionId")String transactionId, HttpSession session) {
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}
		DepositTransactionDTO depositorDTO = service.getDepositTransactionDTO(Integer.parseInt(transactionId));
		return depositorDTO;
	}

	@RequestMapping(value = "/supervisorAllDepositTransactionNotWaiting", method = RequestMethod.POST)
	public @ResponseBody List<DepositTransactionDTO> supervisorAllDepositTransaction(HttpSession session) {
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}
		List<DepositTransactionDTO> depositTransactionDTOs = service.getAllDepositTransactionDTOBySuperVisorNotPending(rs.getEmployee());
		return depositTransactionDTOs;
	}
	
	@RequestMapping(value = "/approveDepositTransaction")
	public String setAprrowDepositTransaction(@RequestParam("transactionId")String transactionId, HttpSession session) {
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}
		DepositTransaction depositTransaction = service.getDepositTransaction(Integer.parseInt(transactionId));
		Date date = new Date();
		if((depositTransaction.getState() != Constant.REJECT) &&  (depositTransaction.getState() != Constant.APPROVE)){
			depositTransaction.setApproveDate(date);
			depositTransaction.setState(Constant.APPROVE);
			depositTransaction.setApproveDate(date);
			AccountInfo accountInfo = depositTransaction.getAccount();
			service.depositAmount(accountInfo, depositTransaction.getAmount());
			service.updateDepositTransaction(depositTransaction);
		}
		
		return "redirect:/supervisor-deposit-transaction.html";
	}
	
	@RequestMapping(value = "/rejectDepositeTransaction")
	public String rejectDepositeTransaction(@RequestParam("transactionId")String transactionId, HttpSession session) {
		
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}
		DepositTransaction depositTransaction = service.getDepositTransaction(Integer.parseInt(transactionId));
		if((depositTransaction.getState() != Constant.REJECT) &&  (depositTransaction.getState() != Constant.APPROVE)){
			depositTransaction.setState(Constant.REJECT);
			depositTransaction.setApproveDate(new Date());
			service.updateDepositTransaction(depositTransaction);
		}
		
		return "redirect:/supervisor-deposit-transaction.html";
	}
	
	//Controller Supervisor End 
}
