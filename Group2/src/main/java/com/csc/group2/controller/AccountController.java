package com.csc.group2.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csc.group2.dto.AccountInfoDTO;
import com.csc.group2.dto.AuthenticationResult;
import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.DepositTransaction;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.InternalPaymentTransaction;
import com.csc.group2.entities.WithdrawalTransaction;
import com.csc.group2.mvc.model.HistoryTransaction;
import com.csc.group2.service.AppService;

@Controller
public class AccountController {

	public static final String DEPOSIT = "Deposit";
	public static final String RECEIVE = "Receive";
	public static final String WITHDRAWAL = "Withdrawal";
	public static final String PAYMENT = "Payment";

	@Autowired
	private AppService service;

	@RequestMapping(value = "/accountDetails")
	public String viewAccountDetail(
			@RequestParam("accountNumber") String accountNumber, Model model,
			HttpSession session) {

		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}

		AccountInfo accountInfo = service.getAccountInfo(accountNumber, rs
				.getEmployee().getId());
		model.addAttribute("account", accountInfo);

		// --------------------------------------------------------

		List<DepositTransaction> listDepositTransaction = service
				.getAllDepositTransactions(accountInfo);
		Employee teller = (Employee) session.getAttribute("employee");
		teller.getTransactionGroup().getSupervisor();

		// --------------------DEPOSIT-----------------

		List<HistoryTransaction> listHistoryTransactions = new ArrayList<HistoryTransaction>();
		for (DepositTransaction depositTransaction : listDepositTransaction) {
			HistoryTransaction historyTransaction = new HistoryTransaction(
					DEPOSIT, depositTransaction.getAmount(), depositTransaction
							.getAccount().getBalance(),
					depositTransaction.getDetail(),
					depositTransaction.getConfirmDate());
			listHistoryTransactions.add(historyTransaction);
		}

		// ---------------WITHDRAW---------------------------

		List<WithdrawalTransaction> listWithTransaction = service
				.getAllWithdrawalTransactions(accountInfo);

		for (WithdrawalTransaction withdrawalTransaction : listWithTransaction) {
			HistoryTransaction historyTransaction = new HistoryTransaction(
					WITHDRAWAL, withdrawalTransaction.getAmount(),
					withdrawalTransaction.getAccount().getBalance(),
					withdrawalTransaction.getDetail(),
					withdrawalTransaction.getConfirmDate());
			listHistoryTransactions.add(historyTransaction);
		}

		// --------------SENDER-----------------------------

		List<InternalPaymentTransaction> listSender = service
				.getAllSender(accountInfo);
		for (InternalPaymentTransaction internalPaymentTransaction : listSender) {
			// HistoryTransaction historyTransaction = new HistoryTransaction(
			// RECEIVE, internalPaymentTransaction.getAmount(),
			// internalPaymentTransaction.getSourceAccount().getBalance(),
			// internalPaymentTransaction.getDetails(),
			// internalPaymentTransaction.getConfirmDate());
			//
			String content = "Pay funds to "
					+ internalPaymentTransaction.getTargetAccount()
							.getFullName();
			HistoryTransaction historyTransaction = new HistoryTransaction(
					content, internalPaymentTransaction.getAmount(),
					internalPaymentTransaction.getSourceAccount().getBalance(),
					internalPaymentTransaction.getDetails(),
					internalPaymentTransaction.getConfirmDate());
			listHistoryTransactions.add(historyTransaction);
		}

		// --------------RECEIVER-----------------------------
		List<InternalPaymentTransaction> listReceiver = service
				.getAllReceiver(accountInfo);
		for (InternalPaymentTransaction internalPaymentTransaction : listReceiver) {
			String content = "Receive funds from "
					+ internalPaymentTransaction.getSourceAccount()
							.getFullName();
			HistoryTransaction historyTransaction = new HistoryTransaction(
					content, internalPaymentTransaction.getAmount(),
					internalPaymentTransaction.getTargetAccount().getBalance(),
					internalPaymentTransaction.getDetails(),
					internalPaymentTransaction.getConfirmDate());
			listHistoryTransactions.add(historyTransaction);
		}

		// ---------------------------------------------------------
		System.out.println(listDepositTransaction.size());
		model.addAttribute("listHistoryTransactions", listHistoryTransactions);
		return "account-details";
	}

	@RequestMapping(value = "/findAccountInfoByIdCard", method = RequestMethod.POST)
	public @ResponseBody List<AccountInfoDTO> findAccountByIdCard(
			@RequestParam("idCardNumber") String idCardNumber, Model model,
			HttpSession session) {

		System.out.println(idCardNumber);
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}

		List<AccountInfo> list = service.getAccountByIdCard(idCardNumber);
		List<AccountInfoDTO> pojoList = new ArrayList<AccountInfoDTO>(
				list.size());
		for (AccountInfo accountInfo : list) {
			pojoList.add(toModel(accountInfo));
		}

		return pojoList;
	}

	public static AccountInfoDTO toModel(AccountInfo accountInfo) {
		AccountInfoDTO accountInfoPOJO = new AccountInfoDTO();
		accountInfoPOJO.setAccountNumber(accountInfo.getAccountNumber());
		accountInfoPOJO.setAccountType(accountInfo.getAccountType());
		accountInfoPOJO.setIdCardNumber(accountInfo.getIdCardNumber());
		accountInfoPOJO.setFullName(accountInfo.getFullName());
		accountInfoPOJO.setPhone(accountInfo.getPhone());
		accountInfoPOJO.setAddress(accountInfo.getAddress());
		accountInfoPOJO.setState(accountInfo.getState());
		accountInfoPOJO.setBank(accountInfo.getBankBranch().getBankName());
		accountInfoPOJO.setBranch(accountInfo.getBankBranch().getBranchName());
		//

		if (accountInfo.getAddress2() == null) {
			accountInfoPOJO.setPhone2("");
		} else {
			accountInfoPOJO.setPhone2(accountInfo.getPhone2());
		}

		if (accountInfo.getAddress2() == null) {
			accountInfoPOJO.setAddress2("");
		} else {
			accountInfoPOJO.setAddress2(accountInfo.getAddress2());
		}

		accountInfoPOJO.setFirstName(accountInfo.getFirstName());
		accountInfoPOJO.setLastName(accountInfo.getMidName());
		accountInfoPOJO.setMidName(accountInfo.getMidName());
		accountInfoPOJO.setBalance(fmt(accountInfo.getBalance()));
		accountInfoPOJO.setEmail(accountInfo.getEmail());
		return accountInfoPOJO;
	}

	public static String fmt(double d) {
		if (d == (long) d)
			return String.format("%d", (long) d);
		else
			return String.format("%s", d);
	}

	@RequestMapping(value = "/loadAccountInfo", method = RequestMethod.POST)
	public @ResponseBody AccountInfoDTO loadAccountInfo(
			@RequestParam("accountNumber") String accountNumber, Model model,
			HttpSession session) {

		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}

		AccountInfo accountInfo = service
				.getAccountInfoByAccountNumber(accountNumber);
		if (accountInfo != null) {
			AccountInfoDTO accountInfoPOJO = new AccountInfoDTO();
			accountInfoPOJO = AccountController.toModel(accountInfo);
			return accountInfoPOJO;
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/search-account-information")
	public String showAccountInfors(Model model, HttpSession session) {
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		}
		SimpleDateFormat sdf = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		String date = sdf.format(new Date());
		model.addAttribute("date", date);

		return "search-account-information";
	}

}
