package com.csc.group2.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.csc.group2.dto.AuthenticationResult;
import com.csc.group2.entities.Employee;
import com.csc.group2.service.AppService;

@Controller
public class HomeController {
	
	@Autowired
	private AppService service;
	
	public AppService getService() {
		return service;
	}

	public void setService(AppService service) {
		this.service = service;
	}
	
	@RequestMapping("/home")
	public String home() {
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@RequestParam("loginId")String loginId,  @RequestParam("password")String password, HttpSession session) {		
		System.out.println(password +" "+loginId);
		
		if(service.checkLogin(loginId, password)) {
			Employee employee = service.getEmployeeByLoginID(loginId);
			service.setLogin(employee ,session, loginId);
			AuthenticationResult result = service.checkAuthentication(session);
			if(result.isTeller()){
				return "redirect:/search-account-information.html";
			} else {
				return "redirect:/supervisor-deposit-transaction.html";
			}
		} else {
			return "login";
		}
	}
	
	@RequestMapping(value = "/logout")
	public String logout(HttpSession session) {		
		session.invalidate();
		
		return "redirect:/home.html";
		
	}

}
