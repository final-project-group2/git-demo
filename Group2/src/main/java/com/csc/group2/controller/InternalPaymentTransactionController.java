package com.csc.group2.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.csc.group2.dto.AuthenticationResult;
import com.csc.group2.dto.InternalPaymentTransactionInfoDTO;
import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.InternalPaymentTransaction;
import com.csc.group2.service.AppService;

@Controller
public class InternalPaymentTransactionController {

	@Autowired
	private AppService service;

	@RequestMapping(value = "/internal-payment-transaction")
	public String showInternalPaymentTransactionsOfTeller(Model model,
			HttpSession session, HttpServletRequest request) {

		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isSupervisor())
				return "redirect:/supervisor-internal-payment-transaction.html";
		}

		String accountNumber = request.getParameter("accountNumber");
		Employee employee = (Employee) session.getAttribute(service.STRING_EM);
		List<InternalPaymentTransaction> internalPaymentTransactions = service.getInternalPaymentTransactionByTellerDate(employee);
		String date = service.getNowDate();
		model.addAttribute("date", date);
		model.addAttribute("transactions", internalPaymentTransactions);
		model.addAttribute("transactionInfo", new InternalPaymentTransactionInfoDTO());
		if(accountNumber != null) {
			AccountInfo  accountInfo = service.getAccountInfoByAccountNumber(accountNumber);
			model.addAttribute("account", accountInfo);
		}
		return "internal-payment-transaction";
	}

	@RequestMapping(value = "/addInternalPaymentTransaction", method = RequestMethod.POST)
	public String addInternalPaymentTransaction(@ModelAttribute("transactionInfo") @Valid InternalPaymentTransactionInfoDTO transactionInfo, BindingResult result, HttpSession session, Model model,
			HttpServletResponse response) {
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isSupervisor())
				return "redirect:/supervisor-internal-payment-transaction.html";
		}

		try {
			List<InternalPaymentTransaction> internalPaymentTransactions = service.getInternalPaymentTransactionByTellerDate(rs.getEmployee());
			if (result.hasErrors()) {
				String date = service.getNowDate();
				model.addAttribute("date", date);
				model.addAttribute("transactions", internalPaymentTransactions);
				model.addAttribute("transactionInfo", transactionInfo);
				return "internal-payment-transaction";
			} else {
				Date date = new Date();
				AccountInfo sourceAccountInfo = service.getAccountInfoByAccountNumber(transactionInfo.getSourceAccount());
				AccountInfo targetAccountInfo = service.getAccountInfoByAccountNumber(transactionInfo.getTargetAccount());
				if (sourceAccountInfo.getBalance() < transactionInfo.getAmount()) {
					model.addAttribute("date", date);
					model.addAttribute("transactions", internalPaymentTransactions);
					model.addAttribute("transactionInfo", transactionInfo);
					String error = "Amount lager than balance";
					model.addAttribute("targetError", error);
					return "internal-payment-transaction";
				}
				Employee employee = (Employee) session.getAttribute("employee");
				InternalPaymentTransaction internalPaymentTransaction = new InternalPaymentTransaction(
						date, targetAccountInfo.getFirstName(),
						transactionInfo.getAmount(), date, null, Constant.PENDING,
						transactionInfo.getDetails(), sourceAccountInfo,
						targetAccountInfo, employee.getBankBranch(),
						targetAccountInfo.getBankBranch(), employee, employee
								.getTransactionGroup().getSupervisor());
				service.addTransaction(internalPaymentTransaction);
			}
		} catch (Exception e) {

		}
		return "redirect:/internal-payment-transaction.html";
	}

	@RequestMapping(value = "/supervisor-internal-payment-transaction")
	public String showInternalPaymentTransactionsOfSupervisor(Model model,
			HttpSession session) {

		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isTeller())
				return "redirect:/internal-payment-transaction.html";
		}

		List<InternalPaymentTransaction> internalPaymentTransactionsWaiting = service.getInternalPaymentTransactionByWaiting();
		List<InternalPaymentTransactionInfoDTO> internalPaymentTransactionsApproved = service.getInternalPaymentTransactionBySupervisor(rs.getEmployee());
		String date = service.getNowDate();
		model.addAttribute("date", date);
		model.addAttribute("transactionsWaiting", internalPaymentTransactionsWaiting);
		model.addAttribute("transactionsApproved", internalPaymentTransactionsApproved);
		return "supervisor-internal-payment-transaction";
	}
	
	@RequestMapping(value = "/supervisor-all-internal-payment-transaction")
	@ResponseBody
	public List<InternalPaymentTransactionInfoDTO> showAllInternalPaymentTransactionsOfSupervisor(Model model, HttpSession session) {
		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return null;
		}

		List<InternalPaymentTransactionInfoDTO> infoDTOs = service.getInternalPaymentTransactionBySupervisor(rs.getEmployee());
		return infoDTOs;
	}

	@RequestMapping(value = "/approveInternalPaymentTransactions")
	public String approveInternalPaymentTransactions(
			@RequestParam("transactionId") int transactionId,
			@RequestParam("date") Date date, HttpSession session) {

		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isTeller())
				return "redirect:/internal-payment-transaction.html";
		}

		InternalPaymentTransaction internalPaymentTransaction = service
				.getInternalPaymentTransactionByIdDate(transactionId, date);
		internalPaymentTransaction.setApproveDate(new Date());
		internalPaymentTransaction.setState(Constant.APPROVE);

		AccountInfo sourceAccount = service
				.getAccountInfoByAccountNumber(internalPaymentTransaction
						.getSourceAccount().getAccountNumber());
		sourceAccount.setBalance(sourceAccount.getBalance()
				- internalPaymentTransaction.getAmount());

		AccountInfo targetAccount = service
				.getAccountInfoByAccountNumber(internalPaymentTransaction
						.getTargetAccount().getAccountNumber());
		targetAccount.setBalance(targetAccount.getBalance()
				+ internalPaymentTransaction.getAmount());

		service.updateAccountInfo(sourceAccount);
		service.updateAccountInfo(targetAccount);
		service.updateInternalPaymentTransaction(internalPaymentTransaction);

		return "redirect:/supervisor-internal-payment-transaction.html";
	}

	@RequestMapping(value = "/rejectInternalPaymentTransactions")
	public String rejectInternalPaymentTransactions(
			@RequestParam("transactionId") int transactionId,
			@RequestParam("date") Date date, HttpSession session) {

		AuthenticationResult rs = service.checkAuthentication(session);
		if (!rs.isLogged()) {
			return rs.getRecommentAction();
		} else {
			if (rs.isTeller())
				return "redirect:/internal-payment-transaction.html";
		}

		InternalPaymentTransaction internalPaymentTransaction = service
				.getInternalPaymentTransactionByIdDate(transactionId, date);
		internalPaymentTransaction.setApproveDate(new Date());
		internalPaymentTransaction.setState(Constant.REJECT);

		service.updateInternalPaymentTransaction(internalPaymentTransaction);

		return "redirect:/supervisor-internal-payment-transaction.html";
	}

}
