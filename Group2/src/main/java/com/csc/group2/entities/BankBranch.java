package com.csc.group2.entities;


import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bank_branch")
public class BankBranch {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "bank_branch_id")
	private int id;

	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name = "branch_name")
	private String branchName;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "state")
	private String state;
	
	@OneToMany(mappedBy = "bankBranch")
	private transient Collection<AccountInfo> listAccountInfo;
	
	@OneToMany(mappedBy = "branch")
	private transient Collection<DepositTransaction> depositTransaction;
	
	
	/**
	 * 
	 */
	public BankBranch() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param id
	 * @param name
	 * @param address
	 * @param phone
	 * @param state
	 */
	public BankBranch(int id, String bankName, String branchName, String address, String phone,
			String state) {
		super();
		this.id = id;
		this.bankName = bankName;
		this.branchName = branchName;
		this.address = address;
		this.phone = phone;
		this.state = state;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	/**
	 * @return the depositTransaction
	 */
	public Collection<DepositTransaction> getDepositTransaction() {
		return depositTransaction;
	}

	/**
	 * @param depositTransaction the depositTransaction to set
	 */
	public void setDepositTransaction(
			Collection<DepositTransaction> depositTransaction) {
		this.depositTransaction = depositTransaction;
	}
	
	
}
