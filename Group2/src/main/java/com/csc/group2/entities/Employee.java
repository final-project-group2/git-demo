package com.csc.group2.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Entity class for Account table. This is a simple POJO class with annotations
 * to define mapping with DB table
 * 
 * @author Tu
 *
 */
@Entity
@Table(name = "employee")
public class Employee {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "employee_id")
	private int id;

	@Column(name = "login_id")
	private String loginId;

	@Column(name = "password")
	private String password;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "email")
	private String email;

	@Column(name = "phone")
	private String phone;

	@ManyToOne
	@JoinColumn(name = "bank_branch_id")
	private BankBranch bankBranch;

	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "transaction_group", referencedColumnName = "id")
	private TransactionGroup transactionGroup;

	@OneToMany(mappedBy = "teller")
	private transient Collection<DepositTransaction> listDepositTransactionsOfTeller;
	
	@OneToMany(mappedBy = "supervisor")
	private transient Collection<DepositTransaction> listDepositTransactionsOfSuperVisor;
	
	/**
	 * 
	 */
	public Employee() {
		super();
	}

	/**
	 * @param id
	 * @param loginId
	 * @param password
	 * @param fullName
	 * @param email
	 * @param phone
	 * @param bankBranch
	 * @param role
	 * @param transactionGroup
	 */
	public Employee(String loginId, String password, String fullName,
			String email, String phone, BankBranch bankBranch, Role role,
			TransactionGroup transactionGroup) {
		super();
		this.loginId = loginId;
		this.password = password;
		this.fullName = fullName;
		this.email = email;
		this.phone = phone;
		this.bankBranch = bankBranch;
		this.role = role;
		this.transactionGroup = transactionGroup;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId
	 *            the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the bankBranch
	 */
	public BankBranch getBankBranch() {
		return bankBranch;
	}

	/**
	 * @param bankBranch
	 *            the bankBranch to set
	 */
	public void setBankBranch(BankBranch bankBranch) {
		this.bankBranch = bankBranch;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * @return the transactionGroup
	 */
	public TransactionGroup getTransactionGroup() {
		return transactionGroup;
	}

	/**
	 * @param transactionGroup
	 *            the transactionGroup to set
	 */
	public void setTransactionGroup(TransactionGroup transactionGroup) {
		this.transactionGroup = transactionGroup;
	}

	/**
	 * @return the listDepositTransactionsOfTeller
	 */
	public Collection<DepositTransaction> getListDepositTransactionsOfTeller() {
		return listDepositTransactionsOfTeller;
	}

	/**
	 * @param listDepositTransactionsOfTeller the listDepositTransactionsOfTeller to set
	 */
	public void setListDepositTransactionsOfTeller(
			Collection<DepositTransaction> listDepositTransactionsOfTeller) {
		this.listDepositTransactionsOfTeller = listDepositTransactionsOfTeller;
	}

	/**
	 * @return the listDepositTransactionsOfSuperVisor
	 */
	public Collection<DepositTransaction> getListDepositTransactionsOfSuperVisor() {
		return listDepositTransactionsOfSuperVisor;
	}

	/**
	 * @param listDepositTransactionsOfSuperVisor the listDepositTransactionsOfSuperVisor to set
	 */
	public void setListDepositTransactionsOfSuperVisor(
			Collection<DepositTransaction> listDepositTransactionsOfSuperVisor) {
		this.listDepositTransactionsOfSuperVisor = listDepositTransactionsOfSuperVisor;
	}


}