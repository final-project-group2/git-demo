package com.csc.group2.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "withdrawal_transaction")
public class WithdrawalTransaction implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "transaction_id")
	private int id;

	@Column(name = "detail")
	private String detail;

	@Column(name = "amount")
	private double amount;

	@Column(name = "create_date")
	private Date confirmDate;

	@Column(name = "approve_date")
	private Date approveDate;

	@Column(name = "date")
	private Date date;

	@Column(name = "state")
	private String state;

	@ManyToOne
	@JoinColumn(name = "account")
	private AccountInfo account;

	@ManyToOne
	@JoinColumn(name = "bankBranch_id")
	private BankBranch branch;

	@ManyToOne
	@JoinColumn(name = "teller")
	private Employee teller;

	@ManyToOne
	@JoinColumn(name = "supervisor")
	private Employee supervisor;

	public WithdrawalTransaction() {
		super();
	}

	/**
	 * @param id
	 * @param detail
	 * @param amount
	 * @param confirmDate
	 * @param approveDateF
	 * @param state
	 * @param account
	 * @param branch
	 * @param teller
	 * @param supervisor
	 */
	public WithdrawalTransaction(int id, String detail, double amount,
			Date confirmDate, Date approveDate, Date date, String state,
			AccountInfo account, BankBranch branch, Employee teller,
			Employee supervisor) {
		super();
		this.id = id;
		this.detail = detail;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDate = approveDate;
		this.state = state;
		this.account = account;
		this.branch = branch;
		this.teller = teller;
		this.supervisor = supervisor;
		this.date = date;
	}

	public WithdrawalTransaction(String detail, double amount,
			Date confirmDate, Date approveDate, Date date, String state,
			AccountInfo account, BankBranch branch, Employee teller,
			Employee supervisor) {
		super();
		this.detail = detail;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDate = approveDate;
		this.state = state;
		this.account = account;
		this.branch = branch;
		this.teller = teller;
		this.supervisor = supervisor;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public double getAmount() {
		return amount;
	}
	
	public String getAmountInFigures(){
		
		return String.format("%,.2f", amount);
	}
	

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDateF) {
		this.approveDate = approveDateF;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public AccountInfo getAccount() {
		return account;
	}

	public void setAccount(AccountInfo account) {
		this.account = account;
	}

	public BankBranch getBranch() {
		return branch;
	}

	public void setBranch(BankBranch branch) {
		this.branch = branch;
	}

	public Employee getTeller() {
		return teller;
	}

	public void setTeller(Employee teller) {
		this.teller = teller;
	}

	public Employee getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
