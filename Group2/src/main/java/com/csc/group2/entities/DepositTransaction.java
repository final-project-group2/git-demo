package com.csc.group2.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "deposit_transaction")
public class DepositTransaction {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "transaction_id")
	private int id;

	@Column(name = "depositor_name")
	private String depositorName;

	@Column(name = "depositor_id_card")
	private String depositorIdCard;

	@Column(name = "depositor_address")
	private String depositorAddress;

	@Column(name = "detail")
	private String detail;

	@Column(name = "amount")
	private Double amount;

	@Column(name = "create_date")
	private Date confirmDate;

	@Column(name = "approve_date")
	private Date approveDate;

	@Column(name = "state")
	private String state;

	@ManyToOne
	@JoinColumn(name = "account")
	private AccountInfo account;

	@ManyToOne
	@JoinColumn(name = "bank_branch_id")
	private BankBranch branch;

	@ManyToOne
	@JoinColumn(name = "teller_id")
	private Employee teller;

	@ManyToOne
	@JoinColumn(name = "supervisor_id")
	private Employee supervisor;

	/**
	 * 
	 */
	public DepositTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param depositorName
	 * @param depositorIdCard
	 * @param depositorAddress
	 * @param detail
	 * @param amount
	 * @param confirmDate
	 * @param approveDate
	 * @param state
	 * @param account
	 * @param branch
	 * @param teller
	 * @param supervisor
	 */
	public DepositTransaction(String depositorName, String depositorIdCard,
			String depositorAddress, String detail, Double amount,
			Date confirmDate, Date approveDate, String state,
			AccountInfo account, BankBranch branch, Employee teller,
			Employee supervisor) {
		super();
		this.depositorName = depositorName;
		this.depositorIdCard = depositorIdCard;
		this.depositorAddress = depositorAddress;
		this.detail = detail;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDate = approveDate;
		this.state = state;
		this.account = account;
		this.branch = branch;
		this.teller = teller;
		this.supervisor = supervisor;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the depositorName
	 */
	public String getDepositorName() {
		return depositorName;
	}

	/**
	 * @param depositorName the depositorName to set
	 */
	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	/**
	 * @return the depositorIdCard
	 */
	public String getDepositorIdCard() {
		return depositorIdCard;
	}

	/**
	 * @param depositorIdCard the depositorIdCard to set
	 */
	public void setDepositorIdCard(String depositorIdCard) {
		this.depositorIdCard = depositorIdCard;
	}

	/**
	 * @return the depositorAddress
	 */
	public String getDepositorAddress() {
		return depositorAddress;
	}

	/**
	 * @param depositorAddress the depositorAddress to set
	 */
	public void setDepositorAddress(String depositorAddress) {
		this.depositorAddress = depositorAddress;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the confirmDate
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * @return the approveDate
	 */
	public Date getApproveDate() {
		return approveDate;
	}

	/**
	 * @param approveDate the approveDate to set
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the account
	 */
	public AccountInfo getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(AccountInfo account) {
		this.account = account;
	}

	/**
	 * @return the branch
	 */
	public BankBranch getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(BankBranch branch) {
		this.branch = branch;
	}

	/**
	 * @return the teller
	 */
	public Employee getTeller() {
		return teller;
	}

	/**
	 * @param teller the teller to set
	 */
	public void setTeller(Employee teller) {
		this.teller = teller;
	}

	/**
	 * @return the supervisor
	 */
	public Employee getSupervisor() {
		return supervisor;
	}

	/**
	 * @param supervisor the supervisor to set
	 */
	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

}
