package com.csc.group2.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="transaction_group")
public class TransactionGroup {

	@Id
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;
	
	@OneToOne
	@JoinColumn(name="supervisor", referencedColumnName = "employee_id")	
	private Employee supervisor;
	
	@OneToMany (mappedBy="transactionGroup")
	private Collection<Employee> tellers;
	
	public TransactionGroup() {
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * @param id
	 * @param name
	 */
	public TransactionGroup(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Employee getSupervisor() {
		return supervisor;
	}


	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}


	public Collection<Employee> getTellers() {
		return tellers;
	}


	public void setTellers(Collection<Employee> tellers) {
		this.tellers = tellers;
	}
	
	
}
