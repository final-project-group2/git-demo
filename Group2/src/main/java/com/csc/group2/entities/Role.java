package com.csc.group2.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

	@Id
	@Column(name = "role_id")
	private int id;

	@Column(name = "role_name")
	private String roleName;

	/**
	 * 
	 */
	public Role() {
		super();
	}

	/**
	 * @param roleId
	 * @param roleName
	 * @param employees
	 */
	public Role(String roleName) {
		super();
		this.roleName = roleName;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the roleName
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * @param roleName
	 *            the roleName to set
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
