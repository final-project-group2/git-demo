package com.csc.group2.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: InternalPaymentTransaction
 *
 */
/**
 * @author Tu
 *
 */
@Entity
@IdClass(value = PrimaryKey.class)
@Table(name = "internal_payment_transaction")
public class InternalPaymentTransaction implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "transaction_id")
	private int id;
	
	@Id
	@Column(name = "date")
	private Date date;

	@Column(name = "target_name")
	private String receiverName;

	@Column(name = "amount")
	private double amount;

	@Column(name = "create_date")
	private Date confirmDate;

	@Column(name = "approve_date")
	private Date approveDate;

	@Column(name = "state")
	private String state;
	
	@Column(name = "detail")
	private String details;
	
	@ManyToOne
	@JoinColumn(name="source_account_id")
	private AccountInfo sourceAccount;
	
	@ManyToOne
	@JoinColumn(name="target_account_id")
	private AccountInfo targetAccount;
	
	@ManyToOne
	@JoinColumn(name="source_bank_branch_id")
	private BankBranch sourceBankBranch;

	@ManyToOne
	@JoinColumn(name="target_bank_branch_id")
	private BankBranch targetBankBranch;
	
	@ManyToOne
	@JoinColumn(name="teller")
	private Employee teller;
	
	@ManyToOne
	@JoinColumn(name="supervisor")
	private Employee supervisor;

	/**
	 * 
	 */
	public InternalPaymentTransaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param date
	 * @param receiverName
	 * @param amount
	 * @param confirmDate
	 * @param approveDate
	 * @param state
	 * @param details
	 * @param sourceAccount
	 * @param targetAccount
	 * @param sourceBankBranch
	 * @param targetBankBranch
	 * @param teller
	 * @param suppervisor
	 */
	public InternalPaymentTransaction(Date date, String receiverName,
			double amount, Date confirmDate, Date approveDate, String state,
			String details, AccountInfo sourceAccount,
			AccountInfo targetAccount, BankBranch sourceBankBranch,
			BankBranch targetBankBranch, Employee teller, Employee suppervisor) {
		super();
		this.date = date;
		this.receiverName = receiverName;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDate = approveDate;
		this.state = state;
		this.details = details;
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.sourceBankBranch = sourceBankBranch;
		this.targetBankBranch = targetBankBranch;
		this.teller = teller;
		this.supervisor = suppervisor;
	}
	
	/**
	 * @param id
	 * @param receiverName
	 * @param amount
	 * @param confirmDate
	 * @param approveDate
	 * @param state
	 * @param details
	 * @param sourceAccount
	 * @param targetAccount
	 * @param sourceBankBranch
	 * @param targetBankBranch
	 * @param teller
	 * @param suppervisor
	 */
	public InternalPaymentTransaction(int id, String receiverName,
			double amount, Date confirmDate, Date approveDate, String state,
			String details, AccountInfo sourceAccount,
			AccountInfo targetAccount, BankBranch sourceBankBranch,
			BankBranch targetBankBranch, Employee teller, Employee suppervisor) {
		super();
		this.id = id;
		this.receiverName = receiverName;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDate = approveDate;
		this.state = state;
		this.details = details;
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.sourceBankBranch = sourceBankBranch;
		this.targetBankBranch = targetBankBranch;
		this.teller = teller;
		this.supervisor = suppervisor;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the receiverName
	 */
	public String getReceiverName() {
		return receiverName;
	}

	/**
	 * @param receiverName the receiverName to set
	 */
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the confirmDate
	 */
	public Date getConfirmDate() {
		return confirmDate;
	}

	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * @return the approveDate
	 */
	public Date getApproveDate() {
		return approveDate;
	}

	/**
	 * @param approveDate the approveDate to set
	 */
	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @return the sourceAccount
	 */
	public AccountInfo getSourceAccount() {
		return sourceAccount;
	}

	/**
	 * @param sourceAccount the sourceAccount to set
	 */
	public void setSourceAccount(AccountInfo sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	/**
	 * @return the targetAccount
	 */
	public AccountInfo getTargetAccount() {
		return targetAccount;
	}

	/**
	 * @param targetAccount the targetAccount to set
	 */
	public void setTargetAccount(AccountInfo targetAccount) {
		this.targetAccount = targetAccount;
	}

	/**
	 * @return the sourceBankBranch
	 */
	public BankBranch getSourceBankBranch() {
		return sourceBankBranch;
	}

	/**
	 * @param sourceBankBranch the sourceBankBranch to set
	 */
	public void setSourceBankBranch(BankBranch sourceBankBranch) {
		this.sourceBankBranch = sourceBankBranch;
	}

	/**
	 * @return the targetBankBranch
	 */
	public BankBranch getTargetBankBranch() {
		return targetBankBranch;
	}

	/**
	 * @param targetBankBranch the targetBankBranch to set
	 */
	public void setTargetBankBranch(BankBranch targetBankBranch) {
		this.targetBankBranch = targetBankBranch;
	}

	/**
	 * @return the teller
	 */
	public Employee getTeller() {
		return teller;
	}

	/**
	 * @param teller the teller to set
	 */
	public void setTeller(Employee teller) {
		this.teller = teller;
	}

	/**
	 * @return the supervisor
	 */
	public Employee getSupervisor() {
		return supervisor;
	}

	/**
	 * @param suppervisor the suppervisor to set
	 */
	public void setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
	}

}
