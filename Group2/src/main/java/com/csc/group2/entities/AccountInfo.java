package com.csc.group2.entities;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * Entity class for Account table. This is a simple POJO class with annotations
 * to define mapping with DB table
 * 
 * @author Tu
 *
 */
@Entity
@Table(name = "account_info")
public class AccountInfo {

	public static String TYPE_DEPOSIT="Deposit Account";
	// accountNumber field maps with accountNumber column in Account table
	// @Id indicates that accountNumber is the primary key for Account table
	
	@Id
	@Column(name = "account_number")
	private String accountNumber;
	
	@Column(name = "account_type")
	private String accountType;
	
	@Column(name = "id_card_number")
	private String idCardNumber;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "mid_name")
	private String midName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "phone1")
	private String phone;
	
	@Column(name = "phone2")
	private String phone2;
	
	@Column(name = "address1")
	private String address;
	
	@Column(name = "address2")
	private String address2;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "balance")
	private double balance;
	
	@ManyToOne
	@JoinColumn(name="bank_branch_id")
	private BankBranch bankBranch;
	
	@OneToMany(mappedBy = "account")
	private transient Collection<DepositTransaction> depositTransactions;

	/**
	 * 
	 */
	public AccountInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param accountNumber
	 * @param accountType
	 * @param idCardNumber
	 * @param firstName
	 * @param midName
	 * @param lastName
	 * @param phone1
	 * @param phone2
	 * @param address1
	 * @param address2
	 * @param email
	 * @param state
	 * @param balance
	 */
	public AccountInfo(String accountNumber, String accountType,
			String idCardNumber, String firstName, String midName,
			String lastName, String phone, String phone2, String address,
			String address2, String email, String state, double balance) {
		super();
		this.accountNumber = accountNumber;
		this.accountType = accountType;
		this.idCardNumber = idCardNumber;
		this.firstName = firstName;
		this.midName = midName;
		this.lastName = lastName;
		this.phone = phone;
		this.phone2 = phone2;
		this.address = address;
		this.address2 = address2;
		this.email = email;
		this.state = state;
		this.balance = balance;
	}

	

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public BankBranch getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(BankBranch bankBranch) {
		this.bankBranch = bankBranch;
	}

	public Collection<DepositTransaction> getDepositTransactions() {
		return depositTransactions;
	}

	public void setDepositTransactions(
			Collection<DepositTransaction> depositTransactions) {
		this.depositTransactions = depositTransactions;
	}

	public String getFullName(){
		String fullName = getFirstName()+" "+ getMidName()+" "+getLastName();
		fullName = fullName.replaceAll("  ", " ");
		return fullName;
	}
}