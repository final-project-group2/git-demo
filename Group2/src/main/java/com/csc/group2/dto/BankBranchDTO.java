package com.csc.group2.dto;

public class BankBranchDTO {

	private int id;
	private String bankName;
	private String branchName;
	private String address;
	private String phone;
	private String state;

	public BankBranchDTO() {
		super();
	}

	public BankBranchDTO(int id, String bankName, String branchName,
			String address, String phone, String state) {
		super();
		this.id = id;
		this.bankName = bankName;
		this.branchName = branchName;
		this.address = address;
		this.phone = phone;
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}
