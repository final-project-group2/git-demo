package com.csc.group2.dto;

import java.util.Date;

public class WithdrawalTransactionDTO {
	private int id;
	private String detail;
	private double amount;
	private Date confirmDate;
	private Date approveDateF;
	private Date state;
	private AccountInfoDTO account;
	private BankBranchDTO branch;
	private EmployeeDTO teller;
	private EmployeeDTO supervisor;
	public WithdrawalTransactionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WithdrawalTransactionDTO(int id, String detail, double amount,
			Date confirmDate, Date approveDateF, Date state,
			AccountInfoDTO account, BankBranchDTO branch,
			EmployeeDTO teller, EmployeeDTO supervisor) {
		super();
		this.id = id;
		this.detail = detail;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDateF = approveDateF;
		this.state = state;
		this.account = account;
		this.branch = branch;
		this.teller = teller;
		this.supervisor = supervisor;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getConfirmDate() {
		return confirmDate;
	}
	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}
	public Date getApproveDateF() {
		return approveDateF;
	}
	public void setApproveDateF(Date approveDateF) {
		this.approveDateF = approveDateF;
	}
	public Date getState() {
		return state;
	}
	public void setState(Date state) {
		this.state = state;
	}
	public AccountInfoDTO getAccount() {
		return account;
	}
	public void setAccount(AccountInfoDTO account) {
		this.account = account;
	}
	public BankBranchDTO getBranch() {
		return branch;
	}
	public void setBranch(BankBranchDTO branch) {
		this.branch = branch;
	}
	public EmployeeDTO getTeller() {
		return teller;
	}
	public void setTeller(EmployeeDTO teller) {
		this.teller = teller;
	}
	public EmployeeDTO getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(EmployeeDTO supervisor) {
		this.supervisor = supervisor;
	}
	
	
}
