package com.csc.group2.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

/**
 * @author Tu
 *
 */
public class InternalPaymentTransactionInfoDTO {

	@NotEmpty
	@Length(min = 13, max = 13)
	@NumberFormat(style = Style.NUMBER)
	private String sourceAccount;
	
	@NotEmpty
	@Length(min = 13, max = 13)
	@NumberFormat(style = Style.NUMBER)
	private String targetAccount;
	
	@Range(min = 10, max = 10000)
	private double amount;
	
	@NotEmpty
	private String details;
	
	private int id;
	private String date;
	private String sourceState;
	private double sourceBalance;
	private String targetState;
	private String state;
	private String targetBranch;
	private String createDate;
	private String approveDate;
	private String teller;
	
	/**
	 * 
	 */
	public InternalPaymentTransactionInfoDTO() {
		super();
	}
	/**
	 * @param sourceAccount
	 * @param targetAccount
	 * @param amount
	 * @param amountInWord
	 * @param details
	 */
	public InternalPaymentTransactionInfoDTO(String sourceAccount, String targetAccount,
			double amount, String details) {
		super();
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.amount = amount;
		this.details = details;
	}
	/**
	 * @return the sourceAccount
	 */
	public String getSourceAccount() {
		return sourceAccount;
	}
	/**
	 * @param sourceAccount the sourceAccount to set
	 */
	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}
	/**
	 * @return the targetAccount
	 */
	public String getTargetAccount() {
		return targetAccount;
	}
	/**
	 * @param targetAccount the targetAccount to set
	 */
	public void setTargetAccount(String targetAccount) {
		this.targetAccount = targetAccount;
	}
	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the sourceState
	 */
	public String getSourceState() {
		return sourceState;
	}
	/**
	 * @param sourceState the sourceState to set
	 */
	public void setSourceState(String sourceState) {
		this.sourceState = sourceState;
	}
	/**
	 * @return the targetState
	 */
	public String getTargetState() {
		return targetState;
	}
	/**
	 * @param targetState the targetState to set
	 */
	public void setTargetState(String targetState) {
		this.targetState = targetState;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the targetBranch
	 */
	public String getTargetBranch() {
		return targetBranch;
	}
	/**
	 * @param targetBranch the targetBranch to set
	 */
	public void setTargetBranch(String targetBranch) {
		this.targetBranch = targetBranch;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate() {
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	/**
	 * @return the approveDate
	 */
	public String getApproveDate() {
		return approveDate;
	}
	/**
	 * @param approveDate the approveDate to set
	 */
	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}
	/**
	 * @return the teller
	 */
	public String getTeller() {
		return teller;
	}
	/**
	 * @param teller the teller to set
	 */
	public void setTeller(String teller) {
		this.teller = teller;
	}
	/**
	 * @return the sourceBalance
	 */
	public double getSourceBalance() {
		return sourceBalance;
	}
	/**
	 * @param sourceBalance the sourceBalance to set
	 */
	public void setSourceBalance(double sourceBalance) {
		this.sourceBalance = sourceBalance;
	}
	
}
