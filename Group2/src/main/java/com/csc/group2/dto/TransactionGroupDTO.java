package com.csc.group2.dto;
public class TransactionGroupDTO {

	private int id;
	private String name;
	private EmployeeDTO supervisor;
	private int[] employeeIds;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EmployeeDTO getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(EmployeeDTO supervisor) {
		this.supervisor = supervisor;
	}

	public int[] getEmployeeIds() {
		return employeeIds;
	}

	public void setEmployeeIds(int[] employeeIds) {
		this.employeeIds = employeeIds;
	}

}
