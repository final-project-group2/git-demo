package com.csc.group2.dto;

import com.csc.group2.entities.Employee;

public class AuthenticationResult {
	private Employee employee;
	boolean logged = false;
	boolean teller = false;
	boolean supervisor = false;
	private String recommentAction;

	public AuthenticationResult() {
		super();
	}

	public AuthenticationResult(boolean logged, boolean teller,
			boolean supervisor, String recommentAction, Employee employee) {
		super();
		this.logged = logged;
		this.teller = teller;
		this.supervisor = supervisor;
		this.recommentAction = recommentAction;
		this.employee = employee;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public boolean isTeller() {
		return teller;
	}

	public void setTeller(boolean teller) {
		this.teller = teller;
	}

	public boolean isSupervisor() {
		return supervisor;
	}

	public void setSupervisor(boolean supervisor) {
		this.supervisor = supervisor;
	}

	public String getRecommentAction() {
		return recommentAction;
	}

	public void setRecommentAction(String recommentAction) {
		this.recommentAction = recommentAction;
	}

}
