package com.csc.group2.dto;

public class EmployeeDTO {
	private int id;
	private String loginId;
	private String password;
	private String fullName;
	private String email;
	private String phone;
	private BankBranchDTO bankBranch;
	private RoleDTO role;
	private TransactionGroupDTO transactionGroup;

	/**
	 * @param id
	 * @param loginId
	 * @param password
	 * @param fullName
	 * @param email
	 * @param phone
	 * @param bankBranch
	 * @param role
	 * @param transactionGroup
	 */
	public EmployeeDTO(int id, String loginId, String password,
			String fullName, String email, String phone,
			BankBranchDTO bankBranch, RoleDTO role,
			TransactionGroupDTO transactionGroup) {
		super();
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.fullName = fullName;
		this.email = email;
		this.phone = phone;
		this.bankBranch = bankBranch;
		this.role = role;
		this.transactionGroup = transactionGroup;
	}

	/**
	 * 
	 */
	public EmployeeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the bankBranch
	 */
	public BankBranchDTO getBankBranch() {
		return bankBranch;
	}

	/**
	 * @param bankBranch the bankBranch to set
	 */
	public void setBankBranch(BankBranchDTO bankBranch) {
		this.bankBranch = bankBranch;
	}

	/**
	 * @return the role
	 */
	public RoleDTO getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(RoleDTO role) {
		this.role = role;
	}

	/**
	 * @return the transactionGroup
	 */
	public TransactionGroupDTO getTransactionGroup() {
		return transactionGroup;
	}

	/**
	 * @param transactionGroup the transactionGroup to set
	 */
	public void setTransactionGroup(TransactionGroupDTO transactionGroup) {
		this.transactionGroup = transactionGroup;
	}
	
	
}
