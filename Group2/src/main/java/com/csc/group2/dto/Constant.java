package com.csc.group2.dto;

public class Constant {

	public final static String PENDING = "Pending";
	public final static String APPROVE = "Approved";
	public final static String REJECT = "Rejected";
	
	public final static String ACTIVE = "Active";
	public final static String DISABLE = "Disabled";
	
	public final static String DEFAULT_DATE_FORMAT = "MMM dd, yyyy";
	
}
