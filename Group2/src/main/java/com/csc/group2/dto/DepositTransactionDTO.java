package com.csc.group2.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

public class DepositTransactionDTO {

	private int transactionId;

	@NotEmpty
	private String depositorName;

	@NotEmpty
	@NumberFormat(style = Style.NUMBER)
	private String depositorIdCard;

	@NotEmpty
	private String depositorAddress;

	@NotEmpty
	private String detail;

	@Range(min = 10, max = 1000000)
	private Double amount;

	@NotEmpty
	@Length(min = 13, max = 13)
	@NumberFormat(style = Style.NUMBER)
	private String account;
	
	private Double balance;

	private String confirmDate;

	private String approveDate;

	private String state;
	
	private String branch;

	private String teller;

	private String supervisor;

	/**
	 * 
	 */
	public DepositTransactionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param transactionId
	 * @param depositorName
	 * @param depositorIdCard
	 * @param depositorAddress
	 * @param detail
	 * @param amount
	 * @param confirmDate
	 * @param approveDate
	 * @param state
	 * @param account
	 * @param branch
	 * @param teller
	 * @param supervisor
	 */
	public DepositTransactionDTO(int transactionId, String depositorName,
			String depositorIdCard, String depositorAddress, String detail,
			Double amount, String confirmDate, String approveDate, String state,
			String account, Double balance, String branch, String teller, String supervisor) {
		super();
		this.transactionId = transactionId;
		this.depositorName = depositorName;
		this.depositorIdCard = depositorIdCard;
		this.depositorAddress = depositorAddress;
		this.detail = detail;
		this.amount = amount;
		this.confirmDate = confirmDate;
		this.approveDate = approveDate;
		this.state = state;
		this.account = account;
		this.balance = balance;
		this.branch = branch;
		this.teller = teller;
		this.supervisor = supervisor;
	}

	/**
	 * @return the transactionId
	 */
	public int getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the depositorName
	 */
	public String getDepositorName() {
		return depositorName;
	}

	/**
	 * @param depositorName the depositorName to set
	 */
	public void setDepositorName(String depositorName) {
		this.depositorName = depositorName;
	}

	/**
	 * @return the depositorIdCard
	 */
	public String getDepositorIdCard() {
		return depositorIdCard;
	}

	/**
	 * @param depositorIdCard the depositorIdCard to set
	 */
	public void setDepositorIdCard(String depositorIdCard) {
		this.depositorIdCard = depositorIdCard;
	}

	/**
	 * @return the depositorAddress
	 */
	public String getDepositorAddress() {
		return depositorAddress;
	}

	/**
	 * @param depositorAddress the depositorAddress to set
	 */
	public void setDepositorAddress(String depositorAddress) {
		this.depositorAddress = depositorAddress;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return the amount
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the confirmDate
	 */
	public String getConfirmDate() {
		return confirmDate;
	}

	/**
	 * @param confirmDate the confirmDate to set
	 */
	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	/**
	 * @return the approveDate
	 */
	public String getApproveDate() {
		return approveDate;
	}

	/**
	 * @param approveDate the approveDate to set
	 */
	public void setApproveDate(String approveDate) {
		this.approveDate = approveDate;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the balance
	 */
	public Double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the teller
	 */
	public String getTeller() {
		return teller;
	}

	/**
	 * @param teller the teller to set
	 */
	public void setTeller(String teller) {
		this.teller = teller;
	}

	/**
	 * @return the supervisor
	 */
	public String getSupervisor() {
		return supervisor;
	}

	/**
	 * @param supervisor the supervisor to set
	 */
	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}
	
}
