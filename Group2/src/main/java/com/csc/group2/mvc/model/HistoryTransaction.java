package com.csc.group2.mvc.model;

import java.util.Date;

public class HistoryTransaction {

	private String message;
	private double amount;
	private double balance;
	private String detail;
	private String sourceAccount;
	private String targetAccount;
	private Date date;

	/**
	 * 
	 */
	public HistoryTransaction() {
		super();
	}

	/**
	 * @param message
	 * @param amount
	 * @param balance
	 * @param detail
	 * @param date
	 */
	public HistoryTransaction(String message, double amount, double balance,
			String detail, Date date) {
		super();
		this.message = message;
		this.amount = amount;
		this.balance = balance;
		this.detail = detail;
		this.date = date;
	}
	
	
	

	/**
	 * @param message
	 * @param amount
	 * @param balance
	 * @param detail
	 * @param sourceAccount
	 * @param targetAccount
	 * @param date
	 */
	public HistoryTransaction(String message, double amount, double balance,
			String detail, String sourceAccount, String targetAccount, Date date) {
		super();
		this.message = message;
		this.amount = amount;
		this.balance = balance;
		this.detail = detail;
		this.sourceAccount = sourceAccount;
		this.targetAccount = targetAccount;
		this.date = date;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return message;
	}

	/**
	 * @param message the type to set
	 */
	public void setType(String message) {
		this.message = message;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the balance
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the sourceAccount
	 */
	public String getSourceAccount() {
		return sourceAccount;
	}

	/**
	 * @param sourceAccount the sourceAccount to set
	 */
	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	/**
	 * @return the targetAccount
	 */
	public String getTargetAccount() {
		return targetAccount;
	}

	/**
	 * @param targetAccount the targetAccount to set
	 */
	public void setTargetAccount(String targetAccount) {
		this.targetAccount = targetAccount;
	}
	
	

}
