package com.csc.group2.mvc.model;

public class WithdrawalTransactionError {
	
	private boolean hasError = false;
	
	String number;
	String amount;
	String detail;
	
	public WithdrawalTransactionError() {
		super();
		// TODO Auto-generated constructor stub
	}
	public WithdrawalTransactionError(boolean hasError, String number,
			String amount, String detail) {
		super();
		this.hasError = hasError;
		this.number = number;
		this.amount = amount;
		this.detail = detail;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	public boolean isHasError() {
		return hasError;
	}
	
	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}
	
}
