package com.csc.group2.dao;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.InternalPaymentTransaction;

@Repository
public class InternalPaymentTransactionDAOImpl extends
		EntityDAOImpl<InternalPaymentTransaction> implements
		InternalPaymentTransactionDAO {

	public InternalPaymentTransactionDAOImpl() {
		super(InternalPaymentTransaction.class);
	}

	public List<InternalPaymentTransaction> getTransactionsByTellerDate(
			Employee teller) {
		String query = "SELECT e FROM "
				+ InternalPaymentTransaction.class.getSimpleName()
				+ " e WHERE e.teller = ?0 and e.date = current_date()";
		List<InternalPaymentTransaction> internalPaymentTransactions = queryEntities(
				query, teller);
		return internalPaymentTransactions;
	}

	public List<InternalPaymentTransaction> getTransactionsByWaiting() {
		String query = "SELECT e FROM "
				+ InternalPaymentTransaction.class.getSimpleName()
				+ " e WHERE e.state = ?0";
		List<InternalPaymentTransaction> internalPaymentTransactions = queryEntities(
				query, Constant.PENDING);
		return internalPaymentTransactions;
	}

	public InternalPaymentTransaction getTransactionByIdDate(int id, Date date) {
		String query = "SELECT e FROM "
				+ InternalPaymentTransaction.class.getSimpleName()
				+ " e WHERE e.id = ?0 and e.date = ?1";
		InternalPaymentTransaction internalPaymentTransaction = queryEntity(
				query, id, date);
		return internalPaymentTransaction;
	}

	public List<InternalPaymentTransaction> getTransactionBySender(
			AccountInfo accountInfo) {
		String query = "SELECT e FROM  InternalPaymentTransaction e WHERE e.sourceAccount = ?0";
		List<InternalPaymentTransaction> internalPaymentTransactions = queryEntities(
				query, accountInfo);
		return internalPaymentTransactions;
	}

	public List<InternalPaymentTransaction> getTransactionByReceiver(
			AccountInfo accountInfo) {
		String query = "SELECT e FROM  InternalPaymentTransaction e WHERE e.targetAccount = ?0";
		List<InternalPaymentTransaction> internalPaymentTransactions = queryEntities(
				query, accountInfo);
		return internalPaymentTransactions;
	}

	public List<InternalPaymentTransaction> getTransactionsBySupervisor(
			Employee supervisor) {
		String query = "SELECT e FROM "
				+ InternalPaymentTransaction.class.getSimpleName()
				+ " e WHERE e.supervisor = ?0 and e.state <> ?1";
		List<InternalPaymentTransaction> internalPaymentTransactions = queryEntities(
				query, supervisor, Constant.PENDING);
		return internalPaymentTransactions;
	}

	public long countInternalPaymentTransactionByTellerDate(Employee teller) {
		String query = "SELECT count(e.id) FROM " + InternalPaymentTransaction.class.getSimpleName() + " e WHERE e.teller = ?0 and e.date = current_date()";
		long rows = countEntities(query, teller);
		return rows;
	}
	
	public long countInternalPaymentTransactionByWaiting() {
		String query = "SELECT count(e.id) FROM " + InternalPaymentTransaction.class.getSimpleName() + " e WHERE e.state = ?0";
		long rows = countEntities(query, Constant.PENDING);
		return rows;
	}

}
