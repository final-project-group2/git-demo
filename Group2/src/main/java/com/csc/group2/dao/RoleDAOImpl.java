package com.csc.group2.dao;

import org.springframework.stereotype.Component;

import com.csc.group2.entities.Role;

@Component
public class RoleDAOImpl extends EntityDAOImpl<Role> implements RoleDAO {

	public RoleDAOImpl() {
		super(Role.class);
	}

}
