package com.csc.group2.dao;

import java.util.Date;
import java.util.List;

import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.InternalPaymentTransaction;

public interface InternalPaymentTransactionDAO extends EntityDAO<InternalPaymentTransaction> {
	
	public List<InternalPaymentTransaction> getTransactionsByTellerDate(Employee loginId);
	
	public List<InternalPaymentTransaction> getTransactionsByWaiting();
	
	public InternalPaymentTransaction getTransactionByIdDate(int id, Date date);

	public List<InternalPaymentTransaction> getTransactionBySender(AccountInfo accountInfo);

	public List<InternalPaymentTransaction> getTransactionByReceiver(AccountInfo accountInfo);
	
	public List<InternalPaymentTransaction> getTransactionsBySupervisor(Employee employee);
	
	public long countInternalPaymentTransactionByTellerDate(Employee teller);
	
	public long countInternalPaymentTransactionByWaiting();
	
}
