package com.csc.group2.dao;

import java.util.List;

public interface EntityDAO<T> {

	/**
	 * @return all accounts from Account table
	 */
	public List<T> getAll();

	public void update(T entity);
	
	public void add(T entity);
	
	public T getEnityById(Object id);

	public List<T> queryEntities(String queryString, Object... objects);

	public T queryEntity(String queryString, Object... objects);
	
	public long countEntities(String queryString, Object... objects);
	
}
