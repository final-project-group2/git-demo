package com.csc.group2.dao;

import java.util.List;

import com.csc.group2.entities.AccountInfo;

public interface AccountInfoDAO extends EntityDAO<AccountInfo> {
	public List<AccountInfo> getAccountByIdCard(String idCardNumber);
	public AccountInfo getAccountInfoByAccountNumber(String accountNumber);
}
