package com.csc.group2.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;

/**
 * DAO class for AccountInfoDAO entity. This class contains all methods that
 * inserts/updates/deletes account info
 * 
 * @author nvu3
 *
 */
@Component
public class AccountInfoDAOImpl extends EntityDAOImpl<AccountInfo> implements AccountInfoDAO{
	
	public AccountInfoDAOImpl() {
		super(AccountInfo.class);
	}

	public List<AccountInfo> getAccountByIdCard(String idCardNumber) {
		String queryString = "SELECT e FROM AccountInfo e WHERE e.idCardNumber = ?0";
		return this.queryEntities(queryString, idCardNumber);
	}

	public AccountInfo getAccountInfoByAccountNumber(String accountNumber) {
		String sql = "SELECT a FROM " + AccountInfo.class.getName() + " a WHERE a.accountNumber = ?0 and a.state = ?1";
		return this.queryEntity(sql, accountNumber, Constant.ACTIVE);
	}
	
}
