package com.csc.group2.dao;

import com.csc.group2.dto.BankBranchDTO;
import com.csc.group2.entities.BankBranch;

public interface BankBranchDAO extends EntityDAO<BankBranch>{

	public BankBranchDTO toBankBranchPOJO(BankBranch bankBranch);
}
