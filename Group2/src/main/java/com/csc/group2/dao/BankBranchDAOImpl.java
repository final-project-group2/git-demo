package com.csc.group2.dao;

import org.springframework.stereotype.Component;

import com.csc.group2.dto.BankBranchDTO;
import com.csc.group2.entities.BankBranch;

@Component
public class BankBranchDAOImpl extends EntityDAOImpl<BankBranch> implements BankBranchDAO{

	public BankBranchDAOImpl() {
		super(BankBranch.class);
	}
	
	public BankBranchDTO toBankBranchPOJO(BankBranch bankBranch){
		BankBranchDTO bankBranchPOJO = 
				new BankBranchDTO(bankBranch.getId(), bankBranch.getBankName(), bankBranch.getBranchName(), 
						bankBranch.getAddress(), bankBranch.getPhone(), bankBranch.getState());
		return bankBranchPOJO;
	}
}
