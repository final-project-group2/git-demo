package com.csc.group2.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class EntityDAOImpl<T> implements EntityDAO<T> {

	@PersistenceContext
	protected EntityManager entityManager;

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public EntityDAOImpl() {
		super();
	}

	public void setEntityManager(EntityManager entityManager) {
		if (entityManager == null)
			this.entityManager = entityManager;
	}

	// T stands for "Type"
	private Class<T> entityClass;

	public Class<T> getEntityClass() {
		return entityClass;
	}

	public void setEntityClass(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public EntityDAOImpl(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	/**
	 * @return all accounts from Account table
	 */
	@Transactional
	public List<T> getAll() {
		String query = "SELECT r FROM " + entityClass.getSimpleName() + " r";
		List<T> entities = entityManager.createQuery(query, entityClass)
				.getResultList();
		return entities;
	}

	@Transactional
	public void add(T entity) {
		entityManager.persist(entity);
	}

	@Transactional
	public void update(T entity) {
		entityManager.merge(entity);
	}

	@Transactional
	public T getEnityById(Object id) {
		T entity = entityManager.find(entityClass, id);
		return entity;
	}

	@Transactional
	public List<T> queryEntities(String queryString, Object... objects) {
		TypedQuery<T> query = entityManager.createQuery(queryString,
				entityClass);
		for (int i = 0; i < objects.length; i++) {
			Object object = objects[i];
			query.setParameter(i, object);
		}
		List<T> entities = query.getResultList();
		return entities;
	}

	@Transactional
	public T queryEntity(String queryString, Object... objects) {
		TypedQuery<T> query = entityManager.createQuery(queryString,
				entityClass);
		for (int i = 0; i < objects.length; i++) {
			Object object = objects[i];
			query.setParameter(i, object);
		}
		T entity = query.getSingleResult();
		return entity;
	}
	
	@Transactional
	public long countEntities(String queryString, Object... objects) {
		TypedQuery<Long> query = entityManager.createQuery(queryString, Long.class);
		for (int i = 0; i < objects.length; i++) {
			Object object = objects[i];
			query.setParameter(i, object);
		}
		long rows = query.getSingleResult();
		return rows;
	}

}
