package com.csc.group2.dao;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.WithdrawalTransaction;

@Component
public class WithdrawalTransactionDAOImpl extends
		EntityDAOImpl<WithdrawalTransaction> implements
		WithdrawalTransactionDAO {

	public WithdrawalTransactionDAOImpl() {
		super(WithdrawalTransaction.class);
	}

	@Transactional
	public void createTransaction(WithdrawalTransaction withdrawalTransaction) {
		this.add(withdrawalTransaction);
	}

	public List<WithdrawalTransaction> getWithdrawalTransactionsBySupervisor(int supervisorId) {
		String queryString = "SELECT t FROM WithdrawalTransaction t WHERE t.supervisor.id = ?0 and (t.date = current_date() and t.state like ?1)";
		queryString += " ORDER BY t.confirmDate DESC";
		return this.queryEntities(queryString, supervisorId, Constant.PENDING);
	}
	
	public List<WithdrawalTransaction> getWithdrawalTransactionsByApprove(int supervisorId) {
		String queryString = "SELECT t FROM WithdrawalTransaction t WHERE t.supervisor.id = ?0 and (t.date = current_date() and t.state <> ?1)";
		queryString += " ORDER BY t.confirmDate DESC";
		return this.queryEntities(queryString, supervisorId, Constant.PENDING);
	}

	public List<WithdrawalTransaction> getWithdrawalTransactionsByTeller(int id) {
		String queryString = "SELECT t FROM WithdrawalTransaction t WHERE t.teller.id = ?0 and (t.date = current_date() or t.state like ?1)";
		queryString += " ORDER BY t.confirmDate DESC";
		return this.queryEntities(queryString, id, Constant.PENDING);
	}

	@Transactional
	public boolean approve(WithdrawalTransaction withdrawalTransaction) {
		
		AccountInfo accountInfo = withdrawalTransaction.getAccount();
		entityManager.merge(withdrawalTransaction);
		entityManager.merge(accountInfo);
		return true;
	}

	public List<WithdrawalTransaction> getAll(AccountInfo accountInfo) {
		String queryString = "SELECT t FROM WithdrawalTransaction t WHERE t.account = ?0)";
		//queryString += " ORDER BY t.confirmDate DESC";
		return this.queryEntities(queryString, accountInfo);
	}

	public long countAllWithdrawalTransaction() {
		String query = "SELECT count(e.id) FROM " + WithdrawalTransaction.class.getSimpleName() + " e";
		long rows = countEntities(query);
		return rows;
	}

	public long countAllWithdrawalTransactionsByTeller(Employee employee) {
		String query = "SELECT count(e.id) FROM " + WithdrawalTransaction.class.getSimpleName() + " e WHERE e.teller = ?0";
		long rows = countEntities(query, employee);
		return rows;
	}

}
