package com.csc.group2.dao;

import java.util.List;

import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.WithdrawalTransaction;

public interface WithdrawalTransactionDAO extends EntityDAO<WithdrawalTransaction> {


	public void createTransaction(WithdrawalTransaction withdrawalTransaction);

	public List<WithdrawalTransaction> getWithdrawalTransactionsBySupervisor(int supervisorId);
	
	public List<WithdrawalTransaction> getWithdrawalTransactionsByApprove(int supervisorId);

	public List<WithdrawalTransaction> getWithdrawalTransactionsByTeller(int id);

	public boolean approve(WithdrawalTransaction withdrawalTransaction);

	public List<WithdrawalTransaction> getAll(AccountInfo accountInfo);
	
	public long countAllWithdrawalTransaction();
	
	public long countAllWithdrawalTransactionsByTeller(Employee employee);

}
