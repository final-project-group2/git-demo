package com.csc.group2.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.DepositTransaction;
import com.csc.group2.entities.Employee;

@Component
public class DepositTransactionDAOImpl extends EntityDAOImpl<DepositTransaction> implements DepositTransactionDAO{

	public DepositTransactionDAOImpl() {
		super(DepositTransaction.class);
		
	}

	public List<DepositTransaction> getAll(AccountInfo accountInfo) {
		String queryString = "SELECT t FROM DepositTransaction t WHERE t.account = ?0)";
		//queryString += " ORDER BY t.confirmDate DESC";
		return this.queryEntities(queryString, accountInfo);
	}

	
	public List<DepositTransaction> getTransactionsByTeller(Employee teller) {
		String query = "SELECT e FROM " + DepositTransaction.class.getSimpleName() + " e WHERE e.teller = ?0 ";
		List<DepositTransaction> depositTransactions = queryEntities(query, teller);
		return depositTransactions;
	}
	
	public List<DepositTransaction> getTransactionsBySupervisorPending(Employee supervisor) {
		String query = "SELECT e FROM " + DepositTransaction.class.getSimpleName() + " e WHERE e.supervisor = ?0 and e.state = ?1";
		List<DepositTransaction> depositTransactions = queryEntities(query, supervisor, Constant.PENDING);
		return depositTransactions;
	}
	public List<DepositTransaction> getTransactionsBySupervisorNotPending(Employee supervisor) {
		String query = "SELECT e FROM " + DepositTransaction.class.getSimpleName() + " e WHERE e.supervisor = ?0 and e.state <> ?1";
		List<DepositTransaction> depositTransactions = queryEntities(query, supervisor, Constant.PENDING);
		return depositTransactions;
	}
	
}
