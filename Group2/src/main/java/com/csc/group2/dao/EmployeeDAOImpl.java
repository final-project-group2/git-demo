package com.csc.group2.dao;

import org.springframework.stereotype.Component;

import com.csc.group2.dto.EmployeeDTO;
import com.csc.group2.entities.Employee;

@Component
public class EmployeeDAOImpl extends EntityDAOImpl<Employee> implements EmployeeDAO{

	public EmployeeDAOImpl() {
		super(Employee.class);
	}

	public boolean checkLogin(String loginId, String password) {

		Employee employee = getEmployeeByLoginID(loginId);
		System.out.println(employee.getLoginId());
		if (employee != null) {
			if (employee.getPassword().equals(password)) {
				return true;
			}
		}
		return false;

	}

	public Employee getEmployeeByLoginID(String loginId) {
		String sql = "SELECT e FROM Employee e WHERE e.loginId = ?0";
		Employee employee = null;
		employee = queryEntity(sql, loginId);
		
		System.out.println(employee.toString());
		return employee;
	}

	public static EmployeeDTO toEmployeePOJO(Employee employee) {
		return null;
	}
}
