package com.csc.group2.dao;

import java.util.List;

import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.DepositTransaction;
import com.csc.group2.entities.Employee;

public interface DepositTransactionDAO extends EntityDAO<DepositTransaction>{

	List<DepositTransaction> getAll(AccountInfo accountInfo);
	public List<DepositTransaction> getTransactionsByTeller(Employee teller);
	public List<DepositTransaction> getTransactionsBySupervisorPending(Employee supervisor);
	public List<DepositTransaction> getTransactionsBySupervisorNotPending(Employee supervisor);

}
