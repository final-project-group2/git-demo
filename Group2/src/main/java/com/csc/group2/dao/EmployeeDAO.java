package com.csc.group2.dao;

import com.csc.group2.entities.Employee;

public interface EmployeeDAO extends EntityDAO<Employee>{
	public boolean checkLogin(String loginId, String password);
	public Employee getEmployeeByLoginID(String loginId);
}
