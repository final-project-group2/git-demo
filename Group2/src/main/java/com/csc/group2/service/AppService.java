package com.csc.group2.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.csc.group2.dao.AccountInfoDAO;
import com.csc.group2.dao.DepositTransactionDAO;
import com.csc.group2.dao.EmployeeDAO;
import com.csc.group2.dao.InternalPaymentTransactionDAO;
import com.csc.group2.dao.RoleDAO;
import com.csc.group2.dao.WithdrawalTransactionDAO;
import com.csc.group2.dto.AuthenticationResult;
import com.csc.group2.dto.DepositTransactionDTO;
import com.csc.group2.dto.InternalPaymentTransactionInfoDTO;
import com.csc.group2.dto.Constant;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.BankBranch;
import com.csc.group2.entities.DepositTransaction;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.InternalPaymentTransaction;
import com.csc.group2.entities.Role;
import com.csc.group2.entities.WithdrawalTransaction;
import com.csc.group2.mvc.model.AccountMdl;
import com.csc.group2.mvc.model.WithdrawalTransactionError;

@Service
public class AppService {

	public final String STRING_EM = "employee";

	@Autowired
	private RoleDAO roleDAO;
	
	@Autowired
	private InternalPaymentTransactionDAO internalPaymentTransactionDAO;

	@Autowired
	private DepositTransactionDAO depositTransactionDAO;

	@Autowired
	private AccountInfoDAO accountInfoDAO;

	@Autowired
	private EmployeeDAO employeeDAO;

	public List<Role> getAllRole() {
		List<Role> roles = roleDAO.getAll();
		return roles;
	}

	@Autowired
	private WithdrawalTransactionDAO withdrawalTransactionDAO;

	// thien code here
	// begin thien code

	protected void saveTransaction(WithdrawalTransaction withdrawalTransaction) {
		withdrawalTransactionDAO.createTransaction(withdrawalTransaction);
	}

	public WithdrawalTransactionError saveTransaction(String detail, double amount,
			Date confirmDate, Date approveDated, Date date, String accountNo,
			int tellerId, int supervisorId) {
		
		WithdrawalTransactionError withdrawalTransactionError = new WithdrawalTransactionError();
		AccountInfo accountInfo = this.accountInfoDAO.getEnityById(accountNo);
		Employee teller = this.employeeDAO.getEnityById(tellerId);
		BankBranch bankBranch = teller.getBankBranch();
		Employee supervisor = employeeDAO.getEnityById(supervisorId);

		if (accountInfo.getBalance() < amount){
			return new WithdrawalTransactionError(true, "", "Amount lager than balance", "");
		}
		
		WithdrawalTransaction withdrawalTransaction = new WithdrawalTransaction(
				detail, amount, confirmDate, approveDated, date, Constant.PENDING,
				accountInfo, bankBranch, teller, supervisor);
		
		
		saveTransaction(withdrawalTransaction);
		return withdrawalTransactionError;
	}

	/**
	 * the one who approved uses this method
	 */
	public boolean approveTransaction(
			WithdrawalTransaction withdrawalTransaction, int supervisorID) {

		if (!withdrawalTransaction.getState().equals(Constant.PENDING)) {
			return false;
		}

		if (withdrawalTransaction.getSupervisor().getId() != supervisorID) {
			return false;
		}

		AccountInfo accountInfo = withdrawalTransaction.getAccount();
		double newValue = accountInfo.getBalance()
				- withdrawalTransaction.getAmount();
		accountInfo.setBalance(newValue);
		withdrawalTransaction.setState(Constant.APPROVE);
		withdrawalTransaction.setApproveDate(new Date());
		withdrawalTransaction.setAccount(accountInfo);

		return withdrawalTransactionDAO.approve(withdrawalTransaction);
	}

	public boolean rejectTransaction(int strasactionID, int supervisorID) {
		WithdrawalTransaction withdrawalTransaction = getWithdrawalTransaction(strasactionID);
		if (!withdrawalTransaction.getState().equals(Constant.PENDING)) {
			return false;
		}

		if (withdrawalTransaction.getSupervisor().getId() != supervisorID) {
			return false;
		}

		withdrawalTransaction.setState(Constant.REJECT);
		withdrawalTransaction.setConfirmDate(new Date());

		withdrawalTransactionDAO.update(withdrawalTransaction);
		return true;
	}

	/**
	 * 
	 * @param withdrawalTransaction
	 * @return
	 */
	public boolean addWithdrawalTransaction(
			WithdrawalTransaction withdrawalTransaction) {
		withdrawalTransactionDAO.add(withdrawalTransaction);
		return true;
	}

	/**
	 * 
	 * @return getAllWithdrawalTransactions
	 */

	public List<WithdrawalTransaction> getAllWithdrawalTransactions() {
		return withdrawalTransactionDAO.getAll();
	}

	public List<WithdrawalTransaction> getAllWithdrawalTransactions(
			AccountInfo accountInfo) {
		return withdrawalTransactionDAO.getAll(accountInfo);
	}

	public List<WithdrawalTransaction> getWithdrawalTransactionsBySupervisor(
			Employee supervisor) {
		return withdrawalTransactionDAO
				.getWithdrawalTransactionsBySupervisor(supervisor.getId());
	}
	
	public List<WithdrawalTransaction> getWithdrawalTransactionsByApprove(int supervisorId) {
		return withdrawalTransactionDAO.getWithdrawalTransactionsByApprove(supervisorId);
	}

	public List<WithdrawalTransaction> getWithdrawalTransactionsByTeller(
			Employee employee) {
		return withdrawalTransactionDAO
				.getWithdrawalTransactionsByTeller(employee.getId());
	}

	public boolean withdrawalAmount(AccountInfo accountInfo, Double amount) {
		boolean result = false;
		accountInfo.setBalance(accountInfo.getBalance() + amount);
		accountInfoDAO.update(accountInfo);
		return result;
	}

	public WithdrawalTransaction getWithdrawalTransaction(int id) {
		return withdrawalTransactionDAO.getEnityById(id);
	}

	public AccountMdl getAccount(String accountNumber, int employeeId) {
		AccountInfo accountInfo = accountInfoDAO.getEnityById(accountNumber);
		String fullName = accountInfo.getFullName();
		AccountMdl accountMdl = new AccountMdl(accountInfo.getAccountNumber(),
				fullName, accountInfo.getBalance(), AccountInfo.TYPE_DEPOSIT);

		return accountMdl;
	}
	
	public long countAllWithdrawalTransaction() {
		long rows = withdrawalTransactionDAO.countAllWithdrawalTransaction();
		return rows;
	}
	
	public long countAllWithdrawalTransactionsByTeller(Employee employee) {
		long rows = withdrawalTransactionDAO.countAllWithdrawalTransactionsByTeller(employee);
		return rows;
	}

	// end thien code

	// tu code here
	// begin tu code
	public List<InternalPaymentTransaction> getInternalPaymentTransactionByTellerDate(
			Employee teller) {
		List<InternalPaymentTransaction> internalPaymentTransactions = internalPaymentTransactionDAO
				.getTransactionsByTellerDate(teller);
		return internalPaymentTransactions;
	}

	public String getNowDate() {
		SimpleDateFormat sdf = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		return sdf.format(new Date());
	}

	public void addTransaction(
			InternalPaymentTransaction internalPaymentTransaction) {
		internalPaymentTransactionDAO.add(internalPaymentTransaction);
		return;
	}

	public List<InternalPaymentTransaction> getInternalPaymentTransactionByWaiting() {
		List<InternalPaymentTransaction> internalPaymentTransactions = internalPaymentTransactionDAO
				.getTransactionsByWaiting();
		return internalPaymentTransactions;
	}

	public List<InternalPaymentTransaction> getAllInternalPaymentTransaction() {
		List<InternalPaymentTransaction> internalPaymentTransactions = internalPaymentTransactionDAO
				.getAll();
		return internalPaymentTransactions;
	}

	public InternalPaymentTransaction getInternalPaymentTransactionByIdDate(
			int id, Date date) {
		InternalPaymentTransaction internalPaymentTransaction = internalPaymentTransactionDAO
				.getTransactionByIdDate(id, date);
		return internalPaymentTransaction;
	}

	public void updateInternalPaymentTransaction(
			InternalPaymentTransaction internalPaymentTransaction) {
		internalPaymentTransactionDAO.update(internalPaymentTransaction);
		return;
	}
	
	public long countInternalPaymentTransactionByTellerDate(Employee teller) {
		return internalPaymentTransactionDAO.countInternalPaymentTransactionByTellerDate(teller);
	}
	
	public long countInternalPaymentTransactionByWaiting() {
		return internalPaymentTransactionDAO.countInternalPaymentTransactionByWaiting();
	}

	// end tu code

	// phuong code here
	// begin phuong code

	public void addDepositTransaction(DepositTransaction depositTransaction) {
		depositTransactionDAO.add(depositTransaction);
	}

	public void updateDepositTransaction(DepositTransaction depositTransaction) {
		depositTransactionDAO.update(depositTransaction);
	}

	public List<DepositTransactionDTO> getAllDepositTransactionsByTeller(Employee teller) {
		List<DepositTransaction> depositTransactions = depositTransactionDAO.getTransactionsByTeller(teller);
		List<DepositTransactionDTO> depositTransactionDTOs = new ArrayList<DepositTransactionDTO>(depositTransactions.size());
		for (DepositTransaction depositTransaction : depositTransactions) {
			depositTransactionDTOs.add(toDepositTransactionDTO(depositTransaction));
		}
		return depositTransactionDTOs;
	}

	public List<DepositTransactionDTO> getAllDepositTransactionsBySuperVisorPending(Employee supervisor) {
		List<DepositTransaction> depositTransactions = depositTransactionDAO.getTransactionsBySupervisorPending(supervisor);
		List<DepositTransactionDTO> depositTransactionDTOs = new ArrayList<DepositTransactionDTO>(depositTransactions.size());
		for (DepositTransaction depositTransaction : depositTransactions) {
			depositTransactionDTOs.add(toDepositTransactionDTO(depositTransaction));
		}
		return depositTransactionDTOs;
	}

	public List<DepositTransactionDTO> getAllDepositTransactionDTOBySuperVisorNotPending(Employee supervisor) {
		List<DepositTransaction> depositTransactions = depositTransactionDAO.getTransactionsBySupervisorNotPending(supervisor);
		List<DepositTransactionDTO> depositTransactionDTOs = new ArrayList<DepositTransactionDTO>(depositTransactions.size());
		for (DepositTransaction depositTransaction : depositTransactions) {
			depositTransactionDTOs.add(toDepositTransactionDTO(depositTransaction));
		}
		return depositTransactionDTOs;
	}

	public DepositTransactionDTO getDepositTransactionDTO(int id) {
		DepositTransaction depositTransaction = depositTransactionDAO.getEnityById(id);
		return toDepositTransactionDTO(depositTransaction);
	}
	
	public DepositTransaction getDepositTransaction(int id) {
		return depositTransactionDAO.getEnityById(id);
	}

	public void depositAmount(AccountInfo accountInfo, Double amount) {
		accountInfo.setBalance(accountInfo.getBalance() + amount);
		accountInfoDAO.update(accountInfo);
	}

	public DepositTransactionDTO toDepositTransactionDTO(DepositTransaction depositTransaction) {
		SimpleDateFormat sdf = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT + " hh:mm:ss");
		String approveDate = "";
		if(depositTransaction.getApproveDate() != null) {
			approveDate = sdf.format(depositTransaction.getApproveDate());
		}
		DepositTransactionDTO depositTransactionDTO = new DepositTransactionDTO(depositTransaction.getId(), depositTransaction.getDepositorName(), 
				depositTransaction.getDepositorIdCard(), depositTransaction.getDepositorAddress(), depositTransaction.getDetail(), 
				depositTransaction.getAmount(), sdf.format(depositTransaction.getConfirmDate()), approveDate,
				depositTransaction.getState(), depositTransaction.getAccount().getAccountNumber(), depositTransaction.getAccount().getBalance(), depositTransaction.getBranch().getBranchName() + " " + depositTransaction.getBranch().getBankName(),
				depositTransaction.getTeller().getLoginId(), depositTransaction.getSupervisor().getLoginId());
		return depositTransactionDTO;
	}

	// end phuong code

	// share code here
	public void setLogin(Employee employee, HttpSession session, String loginId) {

		session.setAttribute(STRING_EM, employee);
	}

	public AuthenticationResult checkAuthentication(HttpSession session) {
		Employee em = (Employee) session.getAttribute(STRING_EM);
		AuthenticationResult rs = new AuthenticationResult(false, false, false,
				"redirect:/home.html", em);
		if (em != null) {
			rs.setLogged(true);

			if (em.getRole().getId() == 1) {
				rs.setTeller(true);
			}

			if (em.getRole().getId() == 2) {
				rs.setSupervisor(true);
			}
		}
		return rs;
	}

	public boolean checkLogin(String loginId, String password) {
		return employeeDAO.checkLogin(loginId, password);
	}

	public Employee getEmployeeByLoginID(String employeeId) {
		Employee employee = employeeDAO.getEmployeeByLoginID(employeeId);
		return employee;
	}
	
	public AccountInfo getAccountInfo(String accountNumber) {
		return accountInfoDAO.getEnityById(accountNumber);
	}
	
	public AccountInfo getAccountInfo(String accountNumber, int employeeId) {
		return accountInfoDAO.getEnityById(accountNumber);
	}

	public AccountInfo getAccountInfoByAccountNumber(String accountNumber) {
		return accountInfoDAO.getAccountInfoByAccountNumber(accountNumber);
	}

	public List<AccountInfo> getAccountByIdCard(String idCardNumber) {
		List<AccountInfo> list = accountInfoDAO
				.getAccountByIdCard(idCardNumber);
		return list;
	}

	public void updateAccountInfo(AccountInfo accountInfo) {
		accountInfoDAO.update(accountInfo);
		return;
	}

	public List<DepositTransaction> getAllDepositTransactions(
			AccountInfo accountInfo) {

		return depositTransactionDAO.getAll(accountInfo);
	}

	public List<InternalPaymentTransaction> getAllSender(AccountInfo accountInfo) {

		return internalPaymentTransactionDAO
				.getTransactionBySender(accountInfo);
	}

	public List<InternalPaymentTransaction> getAllReceiver(
			AccountInfo accountInfo) {

		return internalPaymentTransactionDAO
				.getTransactionByReceiver(accountInfo);
	}

	public List<InternalPaymentTransactionInfoDTO> getInternalPaymentTransactionBySupervisor(Employee employee) {
		List<InternalPaymentTransaction> internalPaymentTransactions = internalPaymentTransactionDAO
				.getTransactionsBySupervisor(employee);
		List<InternalPaymentTransactionInfoDTO> infoDTOs = new ArrayList<InternalPaymentTransactionInfoDTO>(
				internalPaymentTransactions.size());
		for (InternalPaymentTransaction internalPaymentTransaction : internalPaymentTransactions) {
			infoDTOs.add(toModel(internalPaymentTransaction));
		}
		return infoDTOs;
	}

	public static InternalPaymentTransactionInfoDTO toModel(
			InternalPaymentTransaction internalPaymentTransaction) {
		InternalPaymentTransactionInfoDTO infoDTO = new InternalPaymentTransactionInfoDTO();
		infoDTO.setId(internalPaymentTransaction.getId());
		SimpleDateFormat sdf = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT);
		String date = sdf.format(internalPaymentTransaction.getDate());
		infoDTO.setDate(date);
		infoDTO.setSourceAccount(internalPaymentTransaction.getSourceAccount()
				.getAccountNumber());
		infoDTO.setSourceState(internalPaymentTransaction.getSourceAccount()
				.getState());
		infoDTO.setTargetAccount(internalPaymentTransaction.getTargetAccount()
				.getAccountNumber());
		infoDTO.setTargetBranch(internalPaymentTransaction
				.getTargetBankBranch().getBranchName());
		infoDTO.setTargetState(internalPaymentTransaction.getTargetAccount()
				.getState());
		infoDTO.setSourceBalance(internalPaymentTransaction.getSourceAccount()
				.getBalance());
		infoDTO.setAmount(internalPaymentTransaction.getAmount());
		sdf = new SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT + " KK:mm:ss a");
		String createDate = sdf.format(internalPaymentTransaction
				.getConfirmDate());
		infoDTO.setCreateDate(createDate);
		infoDTO.setState(internalPaymentTransaction.getState());
		infoDTO.setTeller(internalPaymentTransaction.getTeller().getLoginId());
		return infoDTO;
	}

	// end share code

}
