<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>


<script type="text/javascript">
	$(document).ready(function() {
		
		$("#mytable").dataTable();
		
		$("#deposit").click(function() {
			window.location.href = "deposit-transaction.html?accountNumber=" + $("#accountNumber").text();
			return false;
		});
		
		$("#transfer").click(function() {
			window.location.href = "internal-payment-transaction.html?accountNumber=" + $("#accountNumber").text();
			return false;
		});
		
		$("#withdrawal").click(function() {
			window.location.href = "withdrawal-transaction.html?accountNumber=" + $("#accountNumber").text();
			return false;
		});

	});
</script>


</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="home.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">
						<li class="active"><a href="search-account-information.html">Search
								Account Information</a></li>
						<li><a href="deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->
			<table id="content-table" class="full-size">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">

							<div id="page-heading" align="center">
								<h1>Account Detail</h1>
							</div>
							<div id="content-table-inner"
								style="padding: 50px 35px 0px 35px;">
								<table width="100%">
										<tr valign="top">
											<td>
												<table id="id-form" width="100%">
													<tr>
														<td>
															<table id="id-form">
																<tr>
																	<th valign="top"><label>Account Number:</label></th>
																	<td><label id="accountNumber">${account.accountNumber}</label></td>
																</tr>

																<tr>
																	<th valign="top"><label>Account Type:</label></th>
																	<td><label>${account.accountType}</label></td>
																</tr>

																<tr>
																	<th valign="top"><label>First Address:</label></th>
																	<td><label>${account.address}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>Second Address:</label></th>
																	<td><label>${account.address2}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>Balance:</label></th>
																	<td><label>${account.balance}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>Bank Branch:</label></th>
																	<td><label>${account.bankBranch.branchName}</label></td>
																</tr>
																
																<tr>
																	<th valign="top"><label>Email:</label></th>
																	<td><label>${account.email}</label></td>
																</tr>
															</table>
														</td>
														<td>
															<table id="id-form">
																<tr>
																	<th valign="top"><label>First Name:</label></th>
																	<td><label>${account.firstName}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>Mid Name:</label></th>
																	<td><label>${account.midName}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>Last Name:</label></th>
																	<td><label>${account.lastName}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>ID Card Number:</label></th>
																	<td><label>${account.idCardNumber}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>First Phone:</label></th>
																	<td><label>${account.phone}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>Second Phone:</label></th>
																	<td><label>${account.phone2}</label></td>
																</tr>
																<tr>
																	<th valign="top"><label>State:</label></th>
																	<td><label>${account.state}</label></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<div align="right">
													<input type="submit" id="deposit" class="btnAdd" value="Deposit" />
													<input type="submit" id="transfer" class="btnAdd" value="Transfer" />
													<input type="submit" id="withdrawal" class="btnAdd" value="Withdraw" />
												</div>
											</td>
										</tr>
										 
									</table>
									<br>
									<table class="table table-hover table-bordered table-striped"
										id="mytable">
											<thead>
												<tr>
													<th>Message</th>
													<th>Amount</th>
													<th>Detail</th>
													<th>Date</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="historyTransactions"
													items="${listHistoryTransactions}">
													<tr bgcolor="green">
														<td>${historyTransactions.type}</td>
														<td>${historyTransactions.amount}</td>
														<td>${historyTransactions.detail}</td>
														<td>${historyTransactions.date}</td>
													</tr>
												</c:forEach>
											</tbody>
									</table>
							</div>
						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>
			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>