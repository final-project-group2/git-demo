<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery/autoNumeric-2.0-BETA.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#mytable").dataTable();
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		 $('#depositorIdCard,#account').keydown(function (e) {
		 	// Allow: backspace, delete, tab, escape, enter and .
       		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	            // Allow: Ctrl+A, Command+A
	           	(e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
	         	// Allow: home, end, left, right, down, up
		    	(e.keyCode >= 35 && e.keyCode <= 40)) {
				// let it happen, don't do anything
				return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		}); 
						 
		$('#amount').autoNumeric('init', {aSign : '$ '});

		$("#depositorName").focusout(function() {
			var val = $(this).val();
			if (val == '') {
				$("#resultCheckDepositor").attr('class', 'error');
				document.getElementById("resultCheckDepositor").innerHTML = "Depositor Not Empty!";
			} else {
				$("#resultCheckDepositor").attr('class', 'ok');
				document.getElementById("resultCheckDepositor").innerHTML = "OK!";
			}
		});

		$("#depositorIdCard").focusout(function() {
			var val = $(this).val();
			if (val == '') {
				$("#resultCheckDepositorIdCard").attr('class', 'error');
				document.getElementById("resultCheckDepositorIdCard").innerHTML = "Id Card Number Not Empty!";
			} else {
				$("#resultCheckDepositorIdCard").attr('class', 'ok');
				document.getElementById("resultCheckDepositorIdCard").innerHTML = "OK!";
			}
		});

		$("#depositorAddress").focusout(function() {
			var val = $(this).val();
			if (val == '') {
				$("#resultCheckDepositorAddress").attr('class', 'error');
				document.getElementById("resultCheckDepositorAddress").innerHTML = "Address Not Empty!";
			} else {
				$("#resultCheckDepositorAddress").attr('class', 'ok');
				document.getElementById("resultCheckDepositorAddress").innerHTML = "OK!";
				}
		});

		$("#account").focusout(function() {
			var val = $(this).val();
			if (val == '') {
				$("#resultCheckAccount").attr('class', 'error');
				document.getElementById("resultCheckAccount").innerHTML = "Account Number Not Empty!";
			} else {
				$.post("loadAccountInfo.html", {accountNumber : $("#account").val()})
				.done(function(data) {
					$("#resultCheckAccount").attr('class', 'ok');
					$("#resultCheckAccount").text("OK!");})
				.fail(function() {
					$("#resultCheckAccount").attr('class', 'error');
					$("#resultCheckAccount").text("Find not Account!");
				});
			}
		});

		$("#amount").focusout(function() {
			var val = $(this).val();
			if (val == '') {
				$("#resultCheckAmount").attr('class', 'error');
				document.getElementById("resultCheckAmount").innerHTML = "Account Number Not Empty!";
			} else {
				$("#resultCheckAmount").attr('class', 'ok');
				document.getElementById("resultCheckAmount").innerHTML = "OK!";
			}
		});

		$('#mytable tbody').on('click','tr',function() {
			var transactionId = $('td', this).eq(0).text();
			var table = $('#mytable').DataTable();
			col = table.row(this).data()
			var cntTable = "";
			$.post("getDetailDepositTransaction.html",{transactionId : transactionId})
				.done(function(data) {
					var depositTransaction = data;
					cntTable = "<tr><th width=\"30%\">Transaction Id</th><td id=\"transactionId\">" + depositTransaction.transactionId + "</td></tr>"
					+ "<tr><th width=\"30%\">Depositor Name</th><td id=\"depositorName\">" + depositTransaction.depositorName + "</td></tr>"
					+ "<tr><th width=\"30%\">Depositor Id Card</th><td id=\"depositorIdCard\">" + depositTransaction.depositorIdCard + "</td></tr>"
					+ "<tr><th width=\"30%\">Depositor Address</th><td id=\"depositorAddress\">" + depositTransaction.depositorAddress + "</td></tr>"
					+ "<tr><th width=\"30%\">Detail</th><td id=\"detail\">" + depositTransaction.detail + "</td></tr>"
					+ "<tr><th width=\"30%\">Amount</th><td id=\"amount\">" + depositTransaction.amount + "</td></tr>"
					+ "<tr><th width=\"30%\">Confirm Date</th><td id=\"confirmDate\">" + depositTransaction.confirmDate + "</td></tr>"
					+ "<tr><th width=\"30%\">Approve Date</th><td id=\"approveDate\">" + depositTransaction.approveDate + "</td></tr>"
					+ "<tr><th width=\"30%\">State</th><td id=\"state\">" + depositTransaction.state + "</td></tr>"
					+ "<tr><th width=\"30%\">Account</th><td id=\"account\">" + depositTransaction.account + "</td></tr>"
					+ "<tr><th width=\"30%\">Account Balance</th><td id=\"account\">" + depositTransaction.balance + "</td></tr>"
					+ "<tr><th width=\"30%\">BankBranch</td><td id=\"bankBranchId\">" + depositTransaction.branch + "</td></tr>"
					+ "<tr><th width=\"30%\">Teller</th><td id=\"tellerId\">" + depositTransaction.teller + "</td></tr>"
					+ "<tr><th width=\"30%\">Supervisor</th><td id=\"supervisorId\">" + depositTransaction.supervisor + "</td></tr>";
					
					$("#myNewTable").html(cntTable);

					$('#myModal').modal('show');

					;})
					.fail(function(error) {
						alert("error");
					});
		});
	});
</script>

<script>
	function checkForm() {
		if (!isNumeric($("#account").val())) {
			$("#resultCheckAccount").attr('class', 'error');
			$("#resultCheckAccount").text("This field is not valid!");
			return false;
		}
		
		var v = $('#amount').autoNumeric('get');
		$('#amount').val(v);
		if (!isNumeric($("#amount").val())) {
			$("#resultCheckAmount").attr('class', 'error');
			$("#resultCheckAmount").text("This field is not valid!");
			return false;
		}
		
		if ($("#depositorName").val() == '') {
			$("#resultCheckDepositor").attr('class', 'error');
			document.getElementById("resultCheckDepositor").innerHTML = "Depositor Not Empty!";
			return false;
		}

		if (!isNumeric($("#depositorIdCard").val())) {
			$("#resultCheckDepositorIdCard").attr('class', 'error');
			document.getElementById("resultCheckDepositorIdCard").innerHTML = "This field is not valid!";
			return false;
		}
		
		if ($('#depositorAddress').val() == '') {
			$("#resultCheckDepositorAddress").attr('class', 'error');
			document.getElementById("resultCheckDepositorAddress").innerHTML = "Address Not Empty!";
			return false;
		}
		
		return true;
	}

	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n) && (n > 0);
	}
</script>

</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="logout.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">
						<li class="active"><a href="search-account-information.html">Search
								Account Information</a></li>
						<li><a href="deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->
			<table border="0" width="100%" cellpadding="0" cellspacing="0"
				id="content-table">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">
							<div id="page-hedingright" align="right">
								<table>
									<tr>
										<th><label>Teller:</label></th>
										<td><label>${employee.loginId}</label></td>
									</tr>
									<tr>
										<th><label>Supervisor:</label></th>
										<td><label>${employee.transactionGroup.supervisor.loginId}</label></td>
									</tr>
								</table>
							</div>
							<div id="page-heading" align="center">
								<h1>Add Deposit Information</h1>
							</div>
							<div align="center" style="padding-top: 15px;">
								<table>
									<tr>
										<th><label>Date:</label></th>
										<td><label>${date}</label></td>
									</tr>
								</table>
							</div>
							<div id="content-table-inner" style="padding-left: 35px;"
								align="center">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr valign="top">
										<td width="30%"></td>
										<td>
											<form:form action="createDepositTransaction.html" method="post" commandName="depositTransactionInfo" modelAttribute="depositTransactionInfo">
												<table border="0" cellpadding="0" cellspacing="0"
													id="id-form">
													<tr>
														<th valign="top">Depositor Name:</th>
														<td><form:input path="depositorName" type="text" class="newinp-form"
															name="depositorName" id="depositorName" /></td>
														<td id="resultCheckDepositor" class="error"><form:errors path="depositorName" cssClass="error" /></td>
													</tr>
													<tr>
														<th valign="top">Id card/PP:</th>
														<td><form:input path="depositorIdCard" type="text" class="newinp-form"
															name="depositorIdCard" id="depositorIdCard" /></td>
														<td id="resultCheckDepositorIdCard" class="error"><form:errors path="depositorIdCard" cssClass="error" /></td>
													</tr>
													<tr>
														<th valign="top">Address:</th>
														<td><form:input path="depositorAddress" type="text" class="newinp-form"
															name="depositorAddress" id="depositorAddress" /></td>
														<td id="resultCheckDepositorAddress" class="error"><form:errors path="depositorAddress" cssClass="error" /></td>
													</tr>
													<tr>
														<th valign="top">Account no:</th>
														<td><form:input path="account" type="text" class="newinp-form"
															name="account" id="account" value="${account.accountNumber}"/></td>
														<td id="resultCheckAccount" class="error"><form:errors path="account" cssClass="error" />${messageError}</td>
													</tr>
													<%-- <tr>
														<td><input type="button" class="btnAdd"
															name="checkInData" id="checkInData" value="Check" /></td>
														<td id="resultCheckAccount" class="error"><form:errors path="account" cssClass="error" />${messageError}</td>
													<tr> --%>
													<tr>
														<th valign="top">Amount:</th>
														<td><form:input path="amount" type="text" class="newinp-form" name="amount"
															id="amount" value="" /></td>
														<td id="resultCheckAmount" class="error"><form:errors path="amount" cssClass="error" /></td>
													</tr>
													<tr>
														<th valign="top">Details:</th>
														<td><form:input path="detail" type="text" class="newinp-form"
															name="detail" id="detail" value="" /></td>
														<td id="resultCheckDetail" class="error"><form:errors path="detail" cssClass="error" /></td>
													</tr>
													<tr>
														<td></td>
														<td align="right"><input type="submit" class="btnAdd"
															value="Deposit" id="addDepositTransaction"
															onclick="return checkForm()" /></td>
														<td></td>
													</tr>
												</table>
											</form:form>
										</td>
									</tr>
								</table>
							</div>
							<table class="table table-hover table-bordered table-striped"
								id="mytable">
								<thead>
									<tr>
										<th>transactionId</th>
										<th>Depositor Name</th>
										<th>Amount</th>
										<th>Confirm Date</th>
										<th>State</th>
										<th>Account</th>
										<th>Account Balance</th>
										<th>Teller</th>
										<th>Supervisor</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="depositTransaction"
										items="${listDepositTransaction}">
										<tr>
											<td>${depositTransaction.transactionId}</td>
											<td>${depositTransaction.depositorName}</td>
											<td>${depositTransaction.amount}</td>
											<td>${depositTransaction.confirmDate}</td>
											<td>${depositTransaction.state}</td>
											<td>${depositTransaction.account}</td>
											<td>${depositTransaction.balance}</td>
											<td>${depositTransaction.teller}</td>
											<td>${depositTransaction.supervisor}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<!-- Modal -->
							<div class="modal fade" id="myModal" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Deposit Transaction Infomation</h4>
										</div>
										<div class="modal-body">
											<table class="table table-hover table-bordered table-striped"
												id="myNewTable">

											</table>
										</div>

										<div class="modal-footer" id="myButton">
											<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>
			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>