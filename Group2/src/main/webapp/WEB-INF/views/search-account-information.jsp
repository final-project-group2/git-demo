<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>


<script type="text/javascript">
	$(document).ready(function() {
		$("#mytable").dataTable();

		$("#findByAccountNumber").click(function() {
			if(!isNumeric($("#accountNumber").val()) || $("#accountNumber").val().length != 13) {
				$("#accountNumberError").text("This field is not empty or must 13 digits!");
				return false;
			}
			$.post("loadAccountInfo.html", {
				accountNumber : $("#accountNumber").val()
			}).done(function(data) {
				if (data == null) {
					alert("find no account");
				} else {
					$("#accountNumberError").text("");
					setSingleRow(data);
				}
			}).fail(function(status) {
				$("#accountNumberError").text("Account not found!");
			});
		});

		$("#findByIDCardNumber").click(function() {
			if(!isNumeric($("#idCardNumber").val()) || $("#idCardNumber").val().length != 9) {
				$("#idCardNumberError").text("This field is not empty or must 9 digits!");
				return false;
			}
			$.post("findAccountInfoByIdCard.html", {
				idCardNumber : $("#idCardNumber").val()
			}).done(function(accounts) {
				if (accounts == null) {
					alert("find no account");
				} else {
					if (accounts.length == 0){
						alert("find no account");
					} else {
						$("#idCardNumberError").text("");
						setMutiRows(accounts);
					}
				}
			}).fail(function(data, status) {
				$("#idCardNumberError").text("Id Card Number not found!");
			});
		});
		
		function isNumeric(n) {
			return !isNaN(parseFloat(n)) && isFinite(n) && (n > 0);
		}
	});
	
	function setSingleRow(account) {
		document.getElementById("tbodyId").innerHTML = makeSingleRow(account);
	}

	function setMutiRows(accounts) {
		var rs = "";
		for (i = 0; i < accounts.length; i++) {
			rs += makeSingleRow(accounts[i]);
		}
		
		document.getElementById("tbodyId").innerHTML = rs;
	}

	function makeSingleRow(data) {
		var tdtd = "</td><td>";
		var htmlString = '<tr><td><a href="accountDetails.html?accountNumber='+data.accountNumber+'">' + data.accountNumber +"</a>"+ tdtd
				+ data.branch + tdtd
				+ data.accountType + tdtd
				+ data.idCardNumber + tdtd
				+ data.fullName + tdtd + data.phone + tdtd 
// 				+ data.phone2 + tdtd
				+ data.address + tdtd 
// 				+ data.address2 + tdtd 
				+ data.email
				+ tdtd + data.state + tdtd + data.balance + '</td></tr>';
		return htmlString;
	}
</script>

</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="logout.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">
						<li class="active"><a href="search-account-information.html">Search
								Account Information</a></li>
						<li><a href="deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->
			
			<table border="0" width="100%" cellpadding="0" cellspacing="0"
				id="content-table">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">
							<div align="right">
								<table>
									<tr>
										<th><label>Teller:</label></th>
										<td><label>${employee.loginId}</label></td>
									</tr>
									<tr>
										<th><label>Supervisor:</label></th>
										<td><label>${employee.transactionGroup.supervisor.loginId}</label></td>
									</tr>
								</table>
							</div>
							<div id="page-heading" align="center">
								<h1>Search Account Information</h1>
							</div>
							<div align="center" style="padding-top: 15px;">
								<table>
									<tr>
										<th><label>Date:</label></th>
										<td><label>${date}</label></td>
									</tr>
								</table>
							</div>
							<div id="content-table-inner" style="padding-top: 15px; padding-left: 35px;">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr valign="top">
										<td>
											<table id="id-form" width="100%">
												<tr>
														<td width="50%">
															<table id="id-form">
																<tr>
																	<th valign="top">Account Number:</th>
																	<td><input id="accountNumber" class="inp-form" /></td>
																	<td id="accountNumberError" class="error"></td>
																</tr>
																<tr>
																	<td></td>
																	<td align="right"><button class="btnAdd" id="findByAccountNumber" type="button" >Load</button></td>
																	<td></td>
																</tr>
															</table>
														</td>
														<td width="50%">
															<table id="id-form">
																<tr>
																	<th valign="top">Id Card Number:</th>
																	<td><input id="idCardNumber" class="inp-form"/></td>
																	<td id="idCardNumberError" class="error"></td>
																</tr>
																<tr>
																	<td></td>
																	<td align="right"><button class="btnAdd" id="findByIDCardNumber" type="button" >Load</button></td>
																	<td></td>
																</tr>
															</table>
														</td>
													</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
							<table id="mytable" class="table table-hover table-bordered table-striped">
								<thead>
									<tr>
										<th>Account Number</th>
										<th>Bank Branch</th>
										<th>Account Type</th>
										<th>ID Card Number</th>
										<th>Full Name</th>
										<th>Phone</th>
<!-- 										<th>Second Phone</th> -->
										<th>Address</th>
<!-- 										<th>Second Address</th> -->
										<th>Email</th>
										<th>State</th>
										<th>Balance</th>
									</tr>
								</thead>
								<tbody id="tbodyId">
									<c:forEach var="account" items="${listAccount}">
										<tr>
											<td>${account.accountNumber}</td>
											<td>${account.branch}</td>
											<td>${account.accountType}</td>
											<td>${account.idCardNumber}</td>
											<td>${account.fullName}</td>
											<td>${account.phone}</td>
<%-- 											<td>${account.phone2}</td> --%>
											<td>${account.address}</td>
<%-- 											<td>${account.address2}</td> --%>
											<td>${account.email}</td>
											<td>${account.state}</td>
											<td>${account.balance}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>
			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>