<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.csc.group2.dto.Constant"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#mytable").dataTable();
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#mytable').dataTable();

		$("#mytableHistory").dataTable();

		$('#mytable tbody, #mytableHistory tbody').on('click','tr',function() {
			var transactionId = $('td', this).eq(0).text();
			var cntTable = "";
			document.getElementById("approve").style.visibility = "hidden";
			document.getElementById("reject").style.visibility = "hidden";
			$.post("getDetailDepositTransaction.html",{transactionId : transactionId})
				.done(function(data) {
					var depositTransaction = data;
					cntTable = 
					  "<tr><th width=\"30%\">Transaction Id</th><td id=\"transactionId\">" + depositTransaction.transactionId + "</td></tr>"
					+ "<tr><th width=\"30%\">Depositor Name</th><td id=\"depositorName\">" + depositTransaction.depositorName + "</td></tr>"
					+ "<tr><th width=\"30%\">Depositor Id Card</th><td id=\"depositorIdCard\">" + depositTransaction.depositorIdCard + "</td></tr>"
					+ "<tr><th width=\"30%\">Depositor Address</th><td id=\"depositorAddress\">" + depositTransaction.depositorAddress + "</td></tr>"
					+ "<tr><th width=\"30%\">Detail</th><td id=\"detail\">" + depositTransaction.detail + "</td></tr>"
					+ "<tr><th width=\"30%\">amount</th><td id=\"amount\">" + depositTransaction.amount + "</td></tr>"
					+ "<tr><th width=\"30%\">Confirm Date</th><td id=\"confirmDate\">" + depositTransaction.confirmDate + "</td></tr>"
					+ "<tr><th width=\"30%\">Approve Date</th><td id=\"approveDate\">" + depositTransaction.approveDate + "</td></tr>"
					+ "<tr><th width=\"30%\">State</th><td id=\"state\">" + depositTransaction.state + "</td></tr>"
					+ "<tr><th width=\"30%\">Account</th><td id=\"account\">" + depositTransaction.account + "</td></tr>"
					+ "<tr><th width=\"30%\">Account Balance</th><td id=\"account\">" + depositTransaction.balance + "</td></tr>"
					+ "<tr><th width=\"30%\">BankBranch Id</td><td id=\"bankBranchId\">" + depositTransaction.branch + "</td></tr>"
					+ "<tr><th width=\"30%\">Teller</th><td id=\"tellerId\">" + depositTransaction.teller + "</td></tr>"
					+ "<tr><th width=\"30%\">Supervisor</th><td id=\"supervisorId\">" + depositTransaction.supervisor + "</td></tr>";
					
					$("#myNewTable").html(cntTable);

					if(depositTransaction.state == "<%=Constant.PENDING%>"){
						document.getElementById("approve").style.visibility = "";
						document.getElementById("reject").style.visibility = "";
					}
					
					$('#myModal').modal('show');

					$("#approve").click(function() {
						window.location.href = "approveDepositTransaction.html?transactionId=" + transactionId;});

					$('#reject').on('click',function() {
						window.location.href = "rejectDepositeTransaction.html?transactionId=" + transactionId;});})
						.fail(function(error) {
							alert("error");
						});
		});
		
		$("#show").click(function() {
			$("#history").show();
		});
		
		$("#hide").click(function() {
			$("#history").hide();
		});

});
</script>
</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="logout.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">
						<li><a href="supervisor-deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="supervisor-internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->
			<table border="0" width="100%" cellpadding="0" cellspacing="0"
				id="content-table">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">
							<div id="page-hedingright" align="right"></div>
							<div id="page-heading" align="center">
								<h1>Approve Deposit Information</h1>
							</div>
							<div align="center" style="padding-top: 15px;">
								<table>
									<tr>
										<th><label>Date:</label></th>
										<td><label>${date}</label></td>
									</tr>
								</table>
							</div>
							<table class="table table-hover table-bordered table-striped"
								id="mytable">
								<thead>
									<tr>
										<th>Transaction Id</th>
										<th>Depositor Name</th>
										<th>Amount</th>
										<th>Confirm Date</th>
										<th>State</th>
										<th>Account Number</th>
										<th>Account Balance</th>
										<th>Teller</th>
										<th>Supervisor</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="depositTransaction"
										items="${listDepositTransactionPending}">
										<tr>
											<td>${depositTransaction.transactionId}</td>
											<td>${depositTransaction.depositorName}</td>
											<td>${depositTransaction.amount}</td>
											<td>${depositTransaction.confirmDate}</td>
											<td>${depositTransaction.state}</td>
											<td>${depositTransaction.account}</td>
											<td>${depositTransaction.balance}</td>
											<td>${depositTransaction.teller}</td>
											<td>${depositTransaction.supervisor}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>

							<button class="btnAdd" id="show" type="button">Show</button>
							<button class="btnAdd" id="hide" type="button">Hide</button>

							<div hidden="hidden" id="history">
								<div align="center"
									style="padding-top: 15px; padding-bottom: 25px;">
									<div align="center">
										<h1>Approve Deposit Transaction History</h1>
									</div>
								</div>

								<table id="mytableHistory" class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>Transaction Id</th>
											<th>Depositor Name</th>
											<th>Amount</th>
											<th>Confirm Date</th>
											<th>Approve Date</th>
											<th>State</th>
											<th>Account</th>
											<th>Account Balance</th>
											<th>Teller</th>
											<th>Supervisor</th>
										</tr>
									</thead>
									<tbody id="tbodyId">
										<c:forEach var="depositTransaction"
										items="${listDepositTransactionNotPending}">
										<tr>
											<td>${depositTransaction.transactionId}</td>
											<td>${depositTransaction.depositorName}</td>
											<td>${depositTransaction.amount}</td>
											<td>${depositTransaction.confirmDate}</td>
											<td>${depositTransaction.approveDate}</td>
											<td>${depositTransaction.state}</td>
											<td>${depositTransaction.account}</td>
											<td>${depositTransaction.balance}</td>
											<td>${depositTransaction.teller}</td>
											<td>${depositTransaction.supervisor}</td>
										</tr>
									</c:forEach>
									</tbody>
								</table>
							</div>

							<!-- Modal -->
							<div class="modal fade" id="myModal" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Deposit Transaction Infomation</h4>
										</div>
										<div class="modal-body">
											<table class="table table-hover table-bordered table-striped"
												id="myNewTable">

											</table>
										</div>

										<div class="modal-footer" id="myButton">
											<button type="button" id="approve" class="btn btn-primary" data-dismiss="modal">Approve</button>
											<button type="button" id="reject" class="btn btn-primary" data-dismiss="modal">Reject</button>
											<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>

						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>
			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>