<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/jquery/numericInput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery/autoNumeric-2.0-BETA.js"></script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$("#mytable").dataTable();
						$("#loadSourceAccountBtn").click(function() {
							if(!isNumeric($("#sourceAccount").val()) || $("#sourceAccount").val().length != 13) {
								$("#sourceError").text("This field is not empty or must 13 digits!");
								return false;
							}
							$.post("loadAccountInfo.html", {accountNumber: $("#sourceAccount").val()})
								.done(function(data) {
									$("#sourceError").text("");
									$("#sourceAccountType").text(data.accountType);
									$("#sourceIdCardNumber").text(data.idCardNumber);
									$("#sourceFullName").text(data.fullName);
									$("#sourcePhone").text(data.phone);
									$("#sourceAddress").text(data.address);
									$("#sourceState").text(data.state);
									$("#sourceBank").text(data.bank);
									$("#sourceBranch").text(data.branch);
									$("#sourceBalance").text(data.balance);
									$("#sender").show();
									$('#transfer').removeAttr('disabled');
								})
								.fail(function() {
									$("#sourceError").text("Account not found!");
									$("#sourceAccountType").text("");
									$("#sourceIdCardNumber").text("");
									$("#sourceFullName").text("");
									$("#sourcePhone").text("");
									$("#sourceAddress").text("");
									$("#sourceState").text("");
									$("#sourceBank").text("");
									$("#sourceBranch").text("");
									$("#sourceBalance").text("");
									$("#sender").hide();
									$("#transfer").attr('disabled','disabled');
								});
						});
						
						$("#loadTargetAccountBtn").click(function() {
							if(!isNumeric($("#targetAccount").val()) || $("#targetAccount").val().length != 13) {
								$("#targetError").text("This field is not empty or must 13 digits!");
								return false;
							}
							$.post("loadAccountInfo.html", {accountNumber: $("#targetAccount").val()})
								.done(function(data) {
									$("#targetError").text("");
									$("#targetAccountType").text(data.accountType);
									$("#targetIdCardNumber").text(data.idCardNumber);
									$("#targetFullName").text(data.fullName);
									$("#targetPhone").text(data.phone);
									$("#targetAddress").text(data.address);
									$("#targetState").text(data.state);
									$("#targetBank").text(data.bank);
									$("#targetBranch").text(data.branch);
									$("#targetBalance").text(data.balance);
									$("#receiver").show();
									$('#transfer').removeAttr('disabled');
								})
								.fail(function() {
									$("#targetError").text("Account not found!");
									$("#targetAccountType").text("");
									$("#targetIdCardNumber").text("");
									$("#targetFullName").text("");
									$("#targetPhone").text("");
									$("#targetAddress").text("");
									$("#targetState").text("");
									$("#targetBank").text("");
									$("#targetBranch").text("");
									$("#targetBalance").text("");
									$("#receiver").hide();
									$("#transfer").attr('disabled','disabled');
								});
						});
						
						$("#amount").focusout(function() {
							$('#amount').autoNumeric('init', {aSign:'$ '});
						});
						
						$("#amount").val("");
						
	});
	jQuery(function($) {
	    $('#amount').autoNumeric('init', {aSign:'$ '});
	});
</script>

<script>
	function checkForm() {
		if(!isNumeric($("#sourceAccount").val()) || $("#sourceAccount").val().length != 13) {
			$("#sourceError").text("This field is not empty or must 13 digits!");
			return false;
		} else {
			$("#sourceError").text("");
			if(!isNumeric($("#targetAccount").val()) || $("#targetAccount").val().length != 13) {
				$("#targetError").text("This field is not empty or must 13 digits!");
				return false;
			} else {
				$("#targetError").text("");
				var v = $('#amount').autoNumeric('get');
				$('#amount').val(v);
				if(!isNumeric($("#amount").val())) {
					$("#amountError").text("This field is not empty or must a positive number!");
					return false;
				} else {
					$("#amountError").text("");
					if($("#details").val().length == 0) {
						$("#detailsError").text("This field is not empty!");
						return false;
					}
				}
			}
		}
		$("#detailsError").text("");
		return true;
	}
	
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n) && (n > 0);
	}
</script>

</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="home.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">
						<li class="active"><a href="search-account-information.html">Search
								Account Information</a></li>
						<li><a href="deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->
			<table id="content-table" class="full-size">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">
							<div align="right">
								<table>
									<tr>
										<th><label>Teller:</label></th>
										<td><label>${employee.loginId}</label></td>
									</tr>
									<tr>
										<th><label>Supervisor:</label></th>
										<td><label>${employee.transactionGroup.supervisor.loginId}</label></td>
									</tr>
								</table>
							</div>
							<div id="page-heading" align="center">
								<h1>Add Internal Payment Information</h1>
							</div>
							<div align="center" style="padding-top: 15px;">
								<table>
									<tr>
										<th><label>Date:</label></th>
										<td><label>${date}</label></td>
									</tr>
								</table>
							</div>
							<div id="content-table-inner" style="padding: 50px 35px 0px 35px;">
								<form:form action="addInternalPaymentTransaction.html" method="post" commandName="transactionInfo" modelAttribute="transactionInfo">
									<table id="id-form" width="100%" >
										<tr valign="top">
											<td>
												<table id="id-form" width="100%">
													<tr>
														<td width="50%">
															<h4>Sender:</h4>
															
															<table id="id-form">
																<tr>
																	<th valign="top">Account Number:</th>
																	<td><form:input path="sourceAccount" id="sourceAccount" class="inp-form" value="${account.accountNumber}" /></td>
																	<td id="sourceError" class="error"><form:errors path="sourceAccount" cssClass="error" />${sourceError}</td>
																</tr>
																<tr>
																	<td></td>
																	<td align="right"><button class="btnAdd" id="loadSourceAccountBtn" type="button" >Details</button></td>
																	<td></td>
																</tr>
															</table>
														</td>
														<td width="50%">
															<h4>Receiver:</h4>
															
															<table id="id-form">
																<tr>
																	<th valign="top">Account Number:</th>
																	<td><form:input path="targetAccount" id="targetAccount" class="inp-form"/></td>
																	<td id="targetError" class="error"><form:errors path="targetAccount" cssClass="error" />${sourceError}</td>
																</tr>
																<tr>
																	<td></td>
																	<td align="right"><button class="btnAdd" id="loadTargetAccountBtn" type="button" >Details</button></td>
																	<td></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
												<table id="id-form" width="100%">
													<tr>
														<td width="50%">
															<table id="sender" hidden="hidden">
																<tr>
																	<th valign="top"><label>Account Type:</label></th>
																	<td><label id="sourceAccountType">${account.accountType}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Id Card Number:</label></th>
																	<td><label id="sourceIdCardNumber">${account.idCardNumber}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Full Name:</label></th>
																	<td><label id="sourceFullName">${account.firstName} ${account.lastName}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Phone:</label></th>
																	<td><label id="sourcePhone">${account.phone}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Address:</label></th>
																	<td><label id="sourceAddress">${account.address}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>State:</label></th>
																	<td><label id="sourceState">${account.state}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Bank:</label></th>
																	<td><label id="sourceBank">${account.bankBranch.bankName}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Branch:</label></th>
																	<td><label id="sourceBranch">${account.bankBranch.branchName}</label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Balance:</label></th>
																	<td><label id="sourceBalance">${account.balance}</label></td>
																	<td></td>
																</tr>
															</table>
														</td>
														<td width="50%">
															<table id="receiver" hidden="hidden">
																<tr>
																	<th valign="top"><label>Account Type:</label></th>
																	<td><label id="targetAccountType"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Id Card Number:</label></th>
																	<td><label id="targetIdCardNumber"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Full Name:</label></th>
																	<td><label id="targetFullName"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Phone:</label></th>
																	<td><label id="targetPhone"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Address:</label></th>
																	<td><label id="targetAddress"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>State:</label></th>
																	<td><label id="targetState"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Bank:</label></th>
																	<td><label id="targetBank"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Branch:</label></th>
																	<td><label id="targetBranch"></label></td>
																	<td></td>
																</tr>
																<tr>
																	<th valign="top"><label>Balance:</label></th>
																	<td><label id="targetBalance"></label></td>
																	<td></td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td><hr style="height: 2px; background-color: blue;"></td>
										</tr>
										<tr>
											<td>
												<table id="id-form">
														<tr>
															<th valign="top">Amount:</th>
															<td><form:input id="amount" path="amount" class="inp-form" /></td>
															<td id="amountError" class="error"><form:errors path="amount" cssClass="error" />${targetError}</td>
														</tr>
														<tr>
															<th valign="top">Details:</th>
															<td><form:textarea id="details" rows="5" cols="28" path="details" /></td>
															<td id="detailsError" class="error"><form:errors path="details" cssClass="error" /></td>
														</tr>
														<tr>
															<td></td>
															<td align="right"><input type="submit" id="transfer" class="btnAdd" value="Transfer" onclick="return checkForm()" /></td>
															<td></td>
														</tr>
													</table>
											</td>
										</tr>
									</table>
								</form:form>
							</div>
							<table id="mytable" class="table table-hover table-bordered table-striped">
								<thead>
									<tr>
										<th>Id</th>
										<th>Source Account</th>
										<th>Source Branch</th>
										<th>Target Account</th>
										<th>Target Branch</th>
										<th>Amount</th>
										<th>Create Date</th>
										<th>Approve Date</th>
										<th>State</th>
										<th>Supervisor</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="transaction" items="${transactions}">
										<tr>
											<td>${transaction.id}</td>
											<td>${transaction.sourceAccount.accountNumber}</td>
											<td>${transaction.sourceBankBranch.branchName}</td>
											<td>${transaction.targetAccount.accountNumber}</td>
											<td>${transaction.targetBankBranch.branchName}</td>
											<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${transaction.amount}" /></td>
											<td><fmt:formatDate type="both" value="${transaction.confirmDate}" /></td>
											<td><fmt:formatDate type="both" value="${transaction.approveDate}" /></td>
											<td>${transaction.state}</td>
											<td>${transaction.supervisor.loginId}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>
			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>