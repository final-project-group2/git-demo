<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/jquery/numericInput.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery/autoNumeric-2.0-BETA.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$("#mytable").dataTable();

						$("#loadAccountBtn").click(function() {
							if(!isNumeric($("#accountNumber").val()) || $("#accountNumber").val().length != 13) {
								$("#error").text("This field is not empty or must 13 digits!");
								return false;
							}
							$.post("loadAccountInfo.html", {
								accountNumber : $("#accountNumber").val()
							}).done(function(data) {
								$("#error").text("");
								$("#accountNameIntput").val(data.fullName);
								$("#bankIdInput").val(data.bank);
								$("#bankBranchIdInput").val(data.branch);
								$("#addAccount").removeAttr('disabled');
							}).fail(function() {
								$("#error").text("Account not found!");
							});
						});
					});
	
	jQuery(function($) {
	    $('#amountInpt').autoNumeric('init', {aSign:'$ '});
	});
</script>


<script>
	function checkForm() {
		if(!isNumeric($("#accountNumber").val()) || $("#accountNumber").val().length != 13) {
			$("#error").text("This field is not empty or must 13 digits!");
			return false;
		} else {
			$("#error").text("");
			var v = $('#amountInpt').autoNumeric('get');
			$('#amountInpt').val(v);
			if(!isNumeric($("#amountInpt").val())) {
				$("#amountError").text("This field is not empty or must a positive number!");
				return false;
			} else {
				$("#amountError").text("");
				if($("#detail").val().length == 0) {
					$("#detailError").text("This field is not empty!");
					return false;
				}
			}
		}
		$("#detailError").text("");
		return true;
	}

	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n) && (n > 0);
	}

	function setDate() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!

		var yyyy = today.getFullYear();
		if (dd < 10) {
			dd = '0' + dd
		}
		if (mm < 10) {
			mm = '0' + mm
		}
		var today = dd + '/' + mm + '/' + yyyy;
		$("#inputDate").val(today);
	}
</script>
</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="home.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">
						<li class="active"><a href="search-account-information.html">Search
								Account Information</a></li>
						<li><a href="deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->

			<table border="0" width="100%" cellpadding="0" cellspacing="0"
				id="content-table">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">
							<div align="right">
								<table>

									<tr></tr>
									<tr>
										<td><label>Teller: </label></td>
										<td><label>${employee.loginId} </label></td>
									</tr>
									<tr>
										<td><label>Supervisor: </label></td>
										<td><label>${employee.transactionGroup.supervisor.loginId}
										</label></td>
									</tr>
								</table>
							</div>
							<div id="page-heading" align="center">
								<h1>Add Withdrawal Information</h1>
							</div>
							<div align="center" style="padding-top: 15px;">
								<table>
									<tr>
										<td><label>Date: </label></td>
										<td><label>${toDate}</label></td>
									</tr>
								</table>
							</div>
							<div id="content-table-inner" style="padding-left: 35px;">
								<table border="0" width="100%" cellpadding="0" cellspacing="0">
									<tr valign="top">
										<td width="35%"></td>
										<td>
											<form action="createWithdrawal.html" method="post">
												<table border="0" cellpadding="0" cellspacing="0"
													id="id-form">

													<tr>
														<th valign="top">Account Number:</th>
														<td><input type="text" id="accountNumber"
															class="inp-form" name="acountNo"
															value="${account.accountNumber}" /></td>
														<td><p id="error" class="error">${error.number}</p></td>

													</tr>
													<tr>
														<td></td>
														<td align="right"><button class="btnAdd"
																id=loadAccountBtn " type="button">Details</button></td>
													</tr>
													<tr>
														<th valign="top"><label>Account Name:</label></th>
														<td><label>${account.fullName}</label></td>
														<td></td>
													</tr>
													<tr>
														<th valign="top"><label>Bank:</label></th>
														<td><label>${employee.bankBranch.bankName}</label></td>
														<td></td>
													</tr>
													<tr>
														<th valign="top"><label>Branch:</label></th>
														<td><label>${employee.bankBranch.branchName}</label></td>
														<td></td>
													</tr>
													<tr>
														<th valign="top">Amount in Figures:</th>
														<td><input id="amountInpt" type="text" class="inp-form"
															name="amountInfigues" /></td>
														<td id="amountError" class="error">${error.amount}</td>
													</tr>
													<tr>
														<th valign="top">Amount in Words:</th>
														<td><input type="text" class="inp-form"
															name="amountInWords" /></td>
														<td></td>
													</tr>
													<tr>
														<th valign="top">Detail:</th>
														<td><textarea id ="detail" name="detail" rows="5" cols="28"></textarea></td>
														<td id="detailError" class="error">${error.detail}</td>
													</tr>
													<tr>
														<td></td>
														<td align="right"><input type="submit" id="addAccount" class="btnAdd" value="Withdraw" onclick="return checkForm()" /></td>
													</tr>
												</table>
												<input type="hidden" name="date" value="${toDate}">
											</form></td>
									</tr>
								</table>
							</div>
							<table id="mytable" class="table table-hover table-bordered table-striped">
								<thead>
									<tr>
										<th>Account Number</th>
										<th>Account Name</th>
										<th>Bank</th>
										<th>Branch</th>
										<th>balance</th>
										<th>Amount</th>
										<th>Detail</th>
										<th>Confirm Date</th>
										<th>Approve Date</th>
										<th>State</th>
										<th>Teller</th>
										<th>Supervisor</th>
									</tr>
								</thead>
								<tbody>
									<c:set var="red" value="red" />
									<c:set var="green" value="green" />
									<c:set var="blue" value="blue" />
									<c:forEach var="withdrawTrans"
										items="${listWithdrawalTransaction}">
										<c:choose>
											<c:when test="${withdrawTrans.state == 'pending' }">
												<c:set var="color" value="${red}" />
											</c:when>
											<c:when test="${withdrawTrans.state == 'rejected' }">
												<c:set var="color" value="${blue}" />
											</c:when>
											<c:otherwise>
												<c:set var="color" value="${green}" />
											</c:otherwise>
										</c:choose>
										
										<tr>
											<td>${withdrawTrans.account.accountNumber}</td>
											<td>${withdrawTrans.account.fullName}</td>
											<td>${withdrawTrans.branch.bankName}</td>
											<td>${withdrawTrans.branch.branchName}</td>
											<td>${withdrawTrans.account.balance}</td>
											<td>${withdrawTrans.amountInFigures}</td>
											<td>${withdrawTrans.detail}</td>
											<td>${withdrawTrans.confirmDate}</td>
											<td>${withdrawTrans.approveDate}</td>

											<td style="color: ${color}">${withdrawTrans.state}</td>
											<td>${withdrawTrans.teller.fullName}</td>
											<td>${withdrawTrans.supervisor.fullName}</td>
										</tr>

									</c:forEach>
								</tbody>
							</table>
						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>

			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>