<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>CSC Banking System</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" />
<link rel="stylesheet" href="css/common.css" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery-ui-min.css" type="text/css" />
<link rel="stylesheet" href="css/jquery.dataTables.css" type="text/css" />

<!-- SCRIPT -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery.tooltip.js" type="text/javascript"></script>
<script src="js/jquery/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.js" type="text/javascript"></script>
<script src="js/jquery/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$("#mytable").dataTable();
						$("#mytable2").dataTable();
						
						var col;
						$('#mytable tbody').on('click','tr',function() {
							var table = $('#mytable').DataTable();
							col = table.row( this ).data();
					        var cntTable = "";
							cntTable = "<tr><th>Id</th><td id=\"id\">" + col[0]
								+ "</td></tr>"
								+ "<tr><th>Account Number</th><td id=\"sourceAccount\">"
								+ col[1] + "</td></tr>"
								+ "<tr><th>Account Name</th><td id=\"sourceAccount\">" + col[2]
								+ "</td></tr>" + "<tr><th>Branch</th><td id=\"sourceAccount\">"
								+ col[3] + "</td></tr>"
								+ "<tr><th>Balance</th><td id=\"sourceBranch\">" + col[4]
								+ "</td></tr>" + "<tr><th>Amount</th><td id=\"targetAccount\">"
								+ col[5] + "</td></tr>"
								+ "<tr><th>Detail</th><td id=\"targetBranch\">" + col[6]
								+ "</td></tr>"
								+ "<tr><th>Confirmed Time</th><td id=\"amount\">" + col[7]
								+ "</td></tr>" + "<tr><th>State</th><td id=\"createDate\">"
								+ col[9] + "</td></tr>"
								+ "<tr><th>Teller</th><td id=\"state\">" + col[10]
								+ "</td></tr>";
							$("#myNewTable").html(cntTable);
					        $('#myModal').modal('show');
						});

						$('#approve').on('click',function() {
							window.location.href = "approveWithdrawalTransaction.html?transactionId=" + col[0];
						});

						$('#reject').on('click',function() {
							window.location.href = "rejectWithdrawalTransaction.html?transactionId=" + col[0];
						});		
						
						$('#show').click(function() {
							$("#history").show();
						});
						
						$('#hide').click(function() {
							$("#history").hide();
						});
						
					});
</script>

</head>

<body>
	<div id="page-top-outer">
		<div class="logout">
			Welcome to <span class="loginName">${employee.loginId}</span> | <a
				href="home.html">Logout</a>
		</div>
		<div id="page-center" style="background-color: white;">

			<!-- start logo -->
			<div id="logo" style="padding-left: 50px">
				<a href=""><img src="images/logo.png" width="229" height="74"
					alt="" /></a>
			</div>

		</div>
	</div>

	<div class="nav-outer-repeat">

		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header" style="width: 80px; height: 37px;">
				</div>
				<div>
					<ul style="width: 700px;" class="nav navbar-nav">

						<li><a href="supervisor-deposit-transaction.html">Deposit
								Transaction</a></li>
						<li><a href="supervisor-internal-payment-transaction.html">Internal
								Payment Transaction</a></li>
						<li><a href="withdrawal-transaction.html">Withdrawal
								Transaction</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>

	<div id="content-outer">
		<!-- start content -->
		<div id="content">

			<!--  start page-heading -->
			<table border="0" width="100%" cellpadding="0" cellspacing="0"
				id="content-table">
				<tr>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowleft.jpg" width="20" height="300"
						alt="" /></th>
					<th class="topleft"></th>
					<td id="tbl-border-top">&nbsp;</td>
					<th class="topright"></th>
					<th rowspan="3" class="sized"><img
						src="images/shared/side_shadowright.jpg" width="20" height="300"
						alt="" /></th>
				</tr>
				<tr>
					<td id="tbl-border-left"></td>
					<td>
						<div id="content-table-inner">
							<div id="page-heading" align="center">
								<h1>Approve Withdrawal Information</h1>
							</div>
							<div align="center" style="padding-top: 15px;">
								<table>
									<tr>
										<th><label>Date:</label></th>
										<td><label>${toDate}</label></td>
									</tr>
								</table>
							</div>
							<div id="content-table-inner" style="padding: 50px 35px 0px 35px;">
								<table id="mytable" class="table table-hover table-bordered table-striped">
									<thead>
										<tr>
											<th>Id</th>
											<th>Account Number</th>
											<th>Account Name</th>
											<th>Branch</th>
											<th>Balance</th>
											<th>Amount</th>
											<th>Detail</th>
											<th>Create Date</th>
<!-- 											<th>Approve Date</th> -->
											<th>State</th>
											<th>Teller</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="withdrawTrans" items="${listPending}">
											<tr>
												<td>${withdrawTrans.id}</td>
												<td>${withdrawTrans.account.accountNumber}</td>
												<td>${withdrawTrans.account.fullName}</td>
												<td>${withdrawTrans.branch.branchName}</td>
												<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${withdrawTrans.account.balance}" /></td>
												<td>${withdrawTrans.amountInFigures}</td>
												<td>${withdrawTrans.detail}</td>
												<td><fmt:formatDate type="both" value="${withdrawTrans.confirmDate}" /></td>
<%-- 												<td>${withdrawTrans.approveDate}</td> --%>
												<td>${withdrawTrans.state}</td>
												<td>${withdrawTrans.teller.fullName}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								
								<button class="btnAdd" id="show" type="button" >Show</button>
								<button class="btnAdd" id="hide" type="button" >Hide</button>
								
								<div hidden="hidden" id="history">
									<div align="center"
										style="padding-top: 15px; padding-bottom: 25px;">
										<div align="center">
											<h1>Approve Withdrawal Transaction History</h1>
										</div>
									</div>
	
									<table id="mytable2" class="table table-hover table-bordered table-striped">
										<thead>
										<tr>
											<th>Id</th>
											<th>Account Number</th>
											<th>Account Name</th>
											<th>Branch</th>
											<th>Balance</th>
											<th>Amount</th>
											<th>Detail</th>
											<th>Create Date</th>
											<th>Approve Date</th>
											<th>State</th>
											<th>Teller</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="withdrawTrans" items="${listApproved}">
											<tr>
												<td>${withdrawTrans.id}</td>
												<td>${withdrawTrans.account.accountNumber}</td>
												<td>${withdrawTrans.account.fullName}</td>
												<td>${withdrawTrans.branch.branchName}</td>
												<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${withdrawTrans.account.balance}" /></td>
												<td>${withdrawTrans.amountInFigures}</td>
												<td>${withdrawTrans.detail}</td>
												<td><fmt:formatDate type="both" value="${withdrawTrans.confirmDate}" /></td>
												<td>${withdrawTrans.approveDate}</td>
												<td>${withdrawTrans.state}</td>
												<td>${withdrawTrans.teller.fullName}</td>
											</tr>
										</c:forEach>
									</tbody>
									</table>
								</div>
							</div>

							<!-- Modal -->
							<div class="modal fade" id="myModal" role="dialog">
								<div class="modal-dialog">

									<!-- Modal content-->
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Withdrawal Transaction Infomation</h4>
										</div>
										<div class="modal-body">
											<table class="table table-hover table-bordered table-striped" id="myNewTable">
												
											</table>
										</div>
										<div class="modal-footer">
											<button type="button" id="approve" class="btn btn-primary" data-dismiss="modal">Approve</button>
											<button type="button" id="reject" class="btn btn-primary" data-dismiss="modal">Reject</button>
											<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
										</div>
									</div>

								</div>
							</div>
						</div>
					</td>
					<td id="tbl-border-right"></td>
				</tr>
				<tr>
					<th class="sized bottomleft"></th>
					<td id="tbl-border-bottom">&nbsp;</td>
					<th class="sized bottomright"></th>
				</tr>
			</table>

			<br />
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<div class="clear">&nbsp;</div>

	<!-- start footer -->
	<div id="footer">
		<!--  start footer-left -->
		<div id="footer-left">
			&copy; Copyright 2014 DAOnJPA| Java Fresher Training Program. <span
				id="spanYear"></span> <a href="http://www.csc.com" target="_blank">www.csc.com</a>
		</div>
		<!--  end footer-left -->
		<div class="clear">&nbsp;</div>
	</div>
</body>

</html>