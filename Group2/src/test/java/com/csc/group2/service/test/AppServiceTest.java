package com.csc.group2.service.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.csc.group2.dao.AccountInfoDAO;
import com.csc.group2.dao.EmployeeDAO;
import com.csc.group2.dto.Constant;
import com.csc.group2.dto.DepositTransactionDTO;
import com.csc.group2.entities.AccountInfo;
import com.csc.group2.entities.DepositTransaction;
import com.csc.group2.entities.Employee;
import com.csc.group2.entities.InternalPaymentTransaction;
import com.csc.group2.entities.WithdrawalTransaction;
import com.csc.group2.service.AppService;

public class AppServiceTest {
	
	AppService service;

	EmployeeDAO employeeDAO;
	
	AccountInfoDAO AccountInfoDAO;
	
	private static final double DELTA = 1e-15;

	
	@Before
	public void setUp() throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("dispatcher-servlet-test.xml");
		service = (AppService) context.getBean("appService");
		employeeDAO = (EmployeeDAO) context.getBean("employeeDAOImpl");
		AccountInfoDAO = (AccountInfoDAO) context.getBean("accountInfoDAOImpl");
	}

	@After
	public void tearDown() throws Exception {
	}

	// share code here
	@Test
	public void checkLoginTest() {
		boolean result = service.checkLogin("phuongpham", "40bd001563085fc35165329ea1ff5c5ecbdbbeef");
		assertTrue(result);
	}
	
	@Test
	public void getEmployeeByLoginIDTest(){
		Employee employee = service.getEmployeeByLoginID("phuongpham");
		assertNotNull(employee);
		assertEquals(3, employee.getId());
	}

	@Test
	public void getAccountInfoByAccountNumberTest(){
		AccountInfo accountInfo = service.getAccountInfoByAccountNumber("6428472263001");
		assertNotNull(accountInfo);
		assertEquals("273401942", accountInfo.getIdCardNumber());
	}
	
	@Test
	public void getAccountByIdCardTest(){
		List<AccountInfo> accountInfos = service.getAccountByIdCard("273401942");
		assertEquals(1, accountInfos.size());
	}

	@Test
	public void updateAccountInfoTest(){
		AccountInfo accountInfo = service.getAccountInfoByAccountNumber("6428472263001");
		accountInfo.setBalance(60000);
		service.updateAccountInfo(accountInfo);
		accountInfo = service.getAccountInfoByAccountNumber("6428472263001");
		assertEquals(60000, accountInfo.getBalance(),DELTA);
	}
	
	// end share code
	
	// phuong code here
	
	@Test
	public void getDepositTransactionTest(){
		DepositTransaction depositTransaction = service.getDepositTransaction(1);
		assertEquals("6703285001850", depositTransaction.getAccount().getAccountNumber());
	}
	
	@Test
	public void getAllDepositTransactionsByTellerTest(){
		Employee teller = employeeDAO.getEmployeeByLoginID("phuongpham");
		List<DepositTransactionDTO> depositTransactions = service.getAllDepositTransactionsByTeller(teller);
		assertEquals(1,depositTransactions.size());
	}
	
	@Test
	public void getAllDepositTransactionsBySuperVisorPendingTest(){
		Employee supervisor = employeeDAO.getEmployeeByLoginID("thieftran");
		List<DepositTransactionDTO> depositTransactions = service.getAllDepositTransactionsBySuperVisorPending(supervisor);
		assertEquals(3, depositTransactions.size());
	}
	
	@Test
	public void getAllDepositTransactionDTOBySuperVisorNotPendingTest(){
		Employee supervisor = employeeDAO.getEmployeeByLoginID("thieftran");
		List<DepositTransactionDTO> depositTransactions = service.getAllDepositTransactionDTOBySuperVisorNotPending(supervisor);
		assertEquals(1, depositTransactions.size());
	}
	
	@Test
	public void depositAmountTest(){
		AccountInfo accountInfo = AccountInfoDAO.getAccountInfoByAccountNumber("6428472263001");
		double balance = accountInfo.getBalance();
		service.depositAmount(accountInfo, 1000.0);
		accountInfo = AccountInfoDAO.getAccountInfoByAccountNumber("6428472263001");
		assertEquals(balance+1000.0, accountInfo.getBalance(), DELTA);
	}
	
	@Test
	public void addDepositTransactionTest(){
		Date date = new Date();
		AccountInfo accountInfo = AccountInfoDAO.getAccountInfoByAccountNumber("6428472263001");
		Employee teller = employeeDAO.getEmployeeByLoginID("phuongpham");
		DepositTransaction depositTransaction = new DepositTransaction("Pham Duy Phuong", "273401942", "CMT8", "no", 100.0,
				date, null, Constant.PENDING, accountInfo, teller.getBankBranch(), teller, teller.getTransactionGroup().getSupervisor());
		service.addDepositTransaction(depositTransaction);
		depositTransaction = service.getDepositTransaction(5);
		assertEquals("6428472263001", depositTransaction.getAccount().getAccountNumber());
	}
	
	@Test
	public void updateDepositTransactionTest(){
		DepositTransaction depositTransaction = service.getDepositTransaction(2);
		depositTransaction.setDepositorIdCard("273401942");
		service.updateDepositTransaction(depositTransaction);
		depositTransaction = service.getDepositTransaction(2);
		assertEquals("273401942", depositTransaction.getDepositorIdCard());
	}
	
	@Test
	public void toDepositTransactionDTOTest(){
		DepositTransaction depositTransaction = service.getDepositTransaction(2);
		DepositTransactionDTO depositTransactionDTO = service.toDepositTransactionDTO(depositTransaction);
		assertEquals( "6703285001850", depositTransactionDTO.getAccount());
	}
	
	// phuong end code
	
	// Tu code here
	// Begin Tu's code
	@Test
	public void getInternalPaymentTransactionByTellerDateTest() {
		Employee tu = employeeDAO.getEmployeeByLoginID("tutranvanspkt");
		List<InternalPaymentTransaction> internalPaymentTransactions = service.getInternalPaymentTransactionByTellerDate(tu);
		long rows = service.countInternalPaymentTransactionByTellerDate(tu);
		long size = internalPaymentTransactions.size();
		assertEquals(rows, size);
	}

	public void addTransaction(InternalPaymentTransaction internalPaymentTransaction) {
		return;
	}

	@Test
	public void getInternalPaymentTransactionByWaitingTest() {
		List<InternalPaymentTransaction> internalPaymentTransactions = service.getInternalPaymentTransactionByWaiting();
		long rows = service.countInternalPaymentTransactionByWaiting();
		long size = internalPaymentTransactions.size();
		assertEquals(rows, size);
	}

	public void updateInternalPaymentTransaction() {
		return;
	}
	// End Tu's code
	
	// Thien code here
	// Begin Thien's code
	@Test
	public void countAllWithdrawalTransaction() {
		List<WithdrawalTransaction> withdrawalTransactions = service.getAllWithdrawalTransactions();
		long rows = service.countAllWithdrawalTransaction();
		long size = withdrawalTransactions.size();
		assertEquals(rows, size);
	}
	
	@Test
	public void countAllWithdrawalTransactionsByTeller() {
		Employee thien = employeeDAO.getEnityById(2);
		List<WithdrawalTransaction> withdrawalTransactions = service.getWithdrawalTransactionsByTeller(thien);
		long rows = service.countAllWithdrawalTransactionsByTeller(thien);
		long size = withdrawalTransactions.size();
		assertEquals(rows, size);
	}
	// End Thien's code
	
}
