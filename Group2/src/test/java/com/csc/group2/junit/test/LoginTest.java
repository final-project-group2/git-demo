package com.csc.group2.junit.test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.csc.group2.entities.Employee;


public class LoginTest {
	ApplicationContext ac;

	@Before
	public void setUp() {
		ac = new FileSystemXmlApplicationContext(
				"file:src/main/webapp/WEB-INF/dispatcher-servlet.xml");
	}

	@Test
	public void testUser() {
		Employee em = (Employee) ac.getBean("user");
		assertEquals(em.getLoginId(), "tutranvanspkt");
	}

	@Test
	public void testPassword() {
		Employee em = (Employee) ac.getBean("password");
		assertEquals(em.getPassword(), "tranvantu");
	}
}
