/* create by thien */


CREATE SCHEMA `banking_deposit_db`;
USE banking_deposit_db;

CREATE TABLE `banking_deposit_db`.`employee` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(63) NOT NULL,
  `password` VARCHAR(127) NOT NULL,
  `fist_name` VARCHAR(63) NOT NULL,
  `last_name` VARCHAR(63) NULL,
  `birthday` DATE NULL,
  `birthplace` VARCHAR(63) NULL,
  `phone` VARCHAR(20) NULL,
  `email` VARCHAR(127) NULL,
  `address` VARCHAR(127) NULL,
  `state` VARCHAR(15) NULL,
  `role_id` INT NULL,
  `permission` BIGINT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `banking_deposit_db`.`role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(63) NOT NULL,
  `default_permission` BIGINT NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `banking_deposit_db`.`account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `account_number` VARCHAR(12) NOT NULL,
  `account_type` INT NULL,
  `id_card_number` VARCHAR(15) NOT NULL,
  `first_name` VARCHAR(63) NOT NULL,
  `last_name` VARCHAR(63) NULL,
  `mid_name` VARCHAR(45) NULL,
  `phone1` VARCHAR(15) NOT NULL,
  `phone2` VARCHAR(45) NULL,
  `address1` VARCHAR(127) NOT NULL,
  `address2` VARCHAR(127) NULL,
  `balance` DOUBLE NOT NULL,
  PRIMARY KEY (`id`));

  CREATE TABLE `banking_deposit_db`.`add_funds_records` (
  `id` INT NOT NULL,
  `account_id` INT NULL,
  `support_id` INT NULL,
  `admin_id` INT NULL,
  `date_time` DATETIME NULL,
  `adding_amount` DOUBLE NULL,
  `balance` DOUBLE NULL,
  `state` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `banking_deposit_db`.`withdraw_funds_records` (
  `id` INT NOT NULL,
  `account_id` INT NULL,
  `support_id` INT NULL,
  `admin_id` INT NULL,
  `date_time` DATETIME NULL,
  `adding_amount` DOUBLE NULL,
  `balance` DOUBLE NULL,
  `state` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE `banking_deposit_db`.`transfer_funds_record` (
  `id` INT NOT NULL,
  `account_id` INT NULL,
  `support_id` INT NULL,
  `admin_id` INT NULL,
  `create_datetime` DATETIME NOT NULL,
  `receive_datetime` DATETIME NULL,
  `transfer_amount` DOUBLE NOT NULL,
  `balance` DOUBLE NOT NULL,
  `receive_account_number` VARCHAR(20) NOT NULL,
  `receiver_name` VARCHAR(127) NOT NULL,
  `massage` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));
  
  ALTER TABLE `banking_deposit_db`.`employee` 
ADD INDEX `fk_role_id_idx` (`role_id` ASC);
ALTER TABLE `banking_deposit_db`.`employee` 
ADD CONSTRAINT `fk_role_id`
  FOREIGN KEY (`role_id`)
  REFERENCES `banking_deposit_db`.`role` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  ALTER TABLE `banking_deposit_db`.`transfer_funds_record` 
ADD INDEX `fk_transfer_account_id_idx` (`account_id` ASC),
ADD INDEX `fk_tramser_support_id_idx` (`support_id` ASC),
ADD INDEX `fk_transfer_admin_id_idx` (`admin_id` ASC);
ALTER TABLE `banking_deposit_db`.`transfer_funds_record` 
ADD CONSTRAINT `fk_transfer_account_id`
  FOREIGN KEY (`account_id`)
  REFERENCES `banking_deposit_db`.`account` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_transser_support_id`
  FOREIGN KEY (`support_id`)
  REFERENCES `banking_deposit_db`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_transfer_admin_id`
  FOREIGN KEY (`admin_id`)
  REFERENCES `banking_deposit_db`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `banking_deposit_db`.`withdraw_funds_records` 
ADD INDEX `fk_withdraw_acount_id_idx` (`account_id` ASC),
ADD INDEX `fk_transfer_support_id_idx` (`support_id` ASC),
ADD INDEX `fk_withdraw_admin_id_idx` (`admin_id` ASC);
ALTER TABLE `banking_deposit_db`.`withdraw_funds_records` 
ADD CONSTRAINT `fk_withdraw_account_id`
  FOREIGN KEY (`account_id`)
  REFERENCES `banking_deposit_db`.`account` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_withdraw_support_id`
  FOREIGN KEY (`support_id`)
  REFERENCES `banking_deposit_db`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_withdraw_admin_id`
  FOREIGN KEY (`admin_id`)
  REFERENCES `banking_deposit_db`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  ALTER TABLE `banking_deposit_db`.`add_funds_records` 
ADD INDEX `fk_add_support_id_idx` (`support_id` ASC),
ADD INDEX `fk_add_account_id_idx` (`account_id` ASC),
ADD INDEX `fk_add_admin_id_idx` (`admin_id` ASC);
ALTER TABLE `banking_deposit_db`.`add_funds_records` 
ADD CONSTRAINT `fk_add_account_id`
  FOREIGN KEY (`account_id`)
  REFERENCES `banking_deposit_db`.`account` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_add_support_id`
  FOREIGN KEY (`support_id`)
  REFERENCES `banking_deposit_db`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_add_admin_id`
  FOREIGN KEY (`admin_id`)
  REFERENCES `banking_deposit_db`.`employee` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




  