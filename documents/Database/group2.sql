-- MySQL dump 10.13  Distrib 5.6.19, for Win64 (x86_64)
--
-- Host: localhost    Database: group2
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_info`
--

DROP TABLE IF EXISTS `account_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_info` (
  `account_number` varchar(15) NOT NULL,
  `account_type` varchar(45) DEFAULT NULL,
  `id_card_number` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `mid_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `address1` varchar(45) DEFAULT NULL,
  `address2` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `bank_branch_id` int(11) NOT NULL,
  PRIMARY KEY (`account_number`),
  KEY `FK_account_bank_idx` (`bank_branch_id`),
  CONSTRAINT `FK_account_bank` FOREIGN KEY (`bank_branch_id`) REFERENCES `bank_branch` (`bank_branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_info`
--

LOCK TABLES `account_info` WRITE;
/*!40000 ALTER TABLE `account_info` DISABLE KEYS */;
INSERT INTO `account_info` VALUES ('0381000334020','Deposit','312154905','Sang','Thanh','Tran','0987346322',NULL,'Tien Giang',NULL,'thanhsangtran92@gmail.com','Active',50400,1),('0381000334021','Deposit','312154905','Sang','Thanh','Tran','0987346322',NULL,'Tien Giang',NULL,'thanhsangtran92@gmail.com','Active',50300,1),('1902834906201','Deposit','301441133','Thien','Huy','Tran','01674753194',NULL,'Long An',NULL,'thieftran@gmail.com','Active',49900,1),('1902834906202','Deposit','301441133','Thien','Huy','Tran','01674753194',NULL,'Long An',NULL,'thieftran@gmail.com','Active',50000,1),('2459874562214','Deposit','352075946','Tu','Van','Tran','0938881951',NULL,'An Giang',NULL,'tutranvanspkt@gmail.com','Active',49700,2),('6428472263001','Deposit','273401942','Phuong','Duy','Pham','0976114221',NULL,'Vung Tau',NULL,'dphuong2k11@gmail.com','Active',62300,1),('6428472263002','Deposit','273401942','Phuong','Duy','Pham','0976114221',NULL,'Vung Tau',NULL,'dphuong2k11@gmail.com','Active',62000,1),('6703285001850','Deposit','352075946','Tu','Van','Tran','0938881951',NULL,'An Giang',NULL,'tutranvanspkt@gmail.com','Active',49800,1);
/*!40000 ALTER TABLE `account_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_branch`
--

DROP TABLE IF EXISTS `bank_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_branch` (
  `bank_branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(45) DEFAULT NULL,
  `branch_name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`bank_branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_branch`
--

LOCK TABLES `bank_branch` WRITE;
/*!40000 ALTER TABLE `bank_branch` DISABLE KEYS */;
INSERT INTO `bank_branch` VALUES (1,'CSC','Tan Binh','364 Cong Hoa, Ward 13, Tan Binh, HCMC','0838272727','1'),(2,'CSC','District 5','366 Nguyen Trai, Ward 8, District 5, HCMC','0838383838','1');
/*!40000 ALTER TABLE `bank_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deposit_transaction`
--

DROP TABLE IF EXISTS `deposit_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deposit_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `depositor_name` varchar(100) DEFAULT NULL,
  `depositor_id_card` varchar(20) DEFAULT NULL,
  `depositor_address` varchar(127) DEFAULT NULL,
  `detail` varchar(45) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `account` varchar(15) NOT NULL,
  `bank_branch_id` int(11) NOT NULL,
  `teller_id` int(11) NOT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `FK_deposit_bankBranch_idx` (`bank_branch_id`),
  KEY `FK_deposit_teller_idx` (`teller_id`),
  KEY `FK_deposit_supervisor_idx` (`supervisor_id`),
  KEY `FK_deposit_account_idx` (`account`),
  CONSTRAINT `FK_deposit_account` FOREIGN KEY (`account`) REFERENCES `account_info` (`account_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_deposit_bankBranch` FOREIGN KEY (`bank_branch_id`) REFERENCES `bank_branch` (`bank_branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_deposit_supervisor` FOREIGN KEY (`supervisor_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_deposit_teller` FOREIGN KEY (`teller_id`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deposit_transaction`
--

LOCK TABLES `deposit_transaction` WRITE;
/*!40000 ALTER TABLE `deposit_transaction` DISABLE KEYS */;
INSERT INTO `deposit_transaction` VALUES (15,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:23','2015-07-24 10:53:08','Approved','6428472263001',1,1,2),(16,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:27','2015-07-24 11:06:43','Approved','6428472263001',1,1,2),(17,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:29','2015-07-24 11:06:48','Rejected','6428472263001',1,1,2),(18,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:30','2015-07-24 11:06:50','Rejected','6428472263001',1,1,2),(19,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:31','2015-07-24 11:06:51','Approved','6428472263001',1,1,2),(20,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:32',NULL,'Pending','6428472263001',1,1,2),(21,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:33',NULL,'Pending','6428472263001',1,1,2),(22,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:34',NULL,'Pending','6428472263001',1,1,2),(23,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:35',NULL,'Pending','6428472263001',1,1,2),(24,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:36',NULL,'Pending','6428472263001',1,1,2),(25,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:37',NULL,'Pending','6428472263001',1,1,2),(26,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:38',NULL,'Pending','6428472263001',1,1,2),(27,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:39',NULL,'Pending','6428472263001',1,1,2),(28,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:41',NULL,'Pending','6428472263001',1,1,2),(29,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:42',NULL,'Pending','6428472263001',1,1,2),(30,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:43',NULL,'Pending','6428472263001',1,1,2),(31,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:44',NULL,'Pending','6428472263001',1,1,2),(32,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:45',NULL,'Pending','6428472263001',1,1,2),(33,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:46',NULL,'Pending','6428472263001',1,1,2),(34,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:47',NULL,'Pending','6428472263001',1,1,2),(35,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:56',NULL,'Pending','6428472263001',1,1,2),(36,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:57',NULL,'Pending','6428472263001',1,1,2),(37,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:43:59',NULL,'Pending','6428472263001',1,1,2),(38,'Phuong','273401942','Vung Tau','Deposit',100,'2015-07-24 10:44:00',NULL,'Pending','6428472263001',1,1,2),(39,'Phuong Duy','273401942','CMT8','Deposit',100,'2015-07-24 11:06:07',NULL,'Pending','6428472263001',1,1,2);
/*!40000 ALTER TABLE `deposit_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `bank_branch_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `transaction_group` int(11) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `FK_employee_bank_branch_id_idx` (`bank_branch_id`),
  KEY `FK_employee_role_id_idx` (`role_id`),
  KEY `FK_employee_transaction_group_idx` (`transaction_group`),
  CONSTRAINT `FK_employee_bankBranch` FOREIGN KEY (`bank_branch_id`) REFERENCES `bank_branch` (`bank_branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_employee_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_employee_transaction_group` FOREIGN KEY (`transaction_group`) REFERENCES `transaction_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'tutranvanspkt','1602be717bf96eb8e7dc4d492ccf9110c5510686','Tu Tran','tutranvanspkt@gmail.com','0938881951',1,1,1),(2,'thieftran','58be37dee4e3cb486d05916f7dccf5674891c650','Thien Tran','thieftran@gmail.com','01674753194',1,2,1),(3,'phuongpham','40bd001563085fc35165329ea1ff5c5ecbdbbeef','Phuong Pham','phamphuong@gmai.com','0938521421',1,1,1),(4,'thanhsangtran','3223ee9f85bbff30ef71ae5263d12c1e8a120e06','Sang Tran','thanhsangtran92@gmail.com','0978178065',1,1,1);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `internal_payment_transaction`
--

DROP TABLE IF EXISTS `internal_payment_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal_payment_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `target_name` varchar(45) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `detail` varchar(45) DEFAULT NULL,
  `source_account_id` varchar(15) NOT NULL,
  `source_bank_branch_id` int(11) NOT NULL,
  `target_account_id` varchar(15) NOT NULL,
  `target_bank_branch_id` int(11) NOT NULL,
  `teller` int(11) NOT NULL,
  `supervisor` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`,`date`),
  KEY `FK_payment_sourceAccount_idx` (`source_account_id`),
  KEY `FK_payment_targetAccount_idx` (`target_account_id`),
  KEY `FK_payment_bankBranch_idx` (`target_bank_branch_id`),
  KEY `FK_payment_teller_idx` (`teller`),
  KEY `FK_payment_supervisor_idx` (`supervisor`),
  KEY `FK_payment_targetBankBranch_idx` (`source_bank_branch_id`),
  CONSTRAINT `FK_payment_sourceAccount` FOREIGN KEY (`source_account_id`) REFERENCES `account_info` (`account_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_payment_sourceBankBranch` FOREIGN KEY (`source_bank_branch_id`) REFERENCES `bank_branch` (`bank_branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_payment_supervisor` FOREIGN KEY (`supervisor`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_payment_targetAccount` FOREIGN KEY (`target_account_id`) REFERENCES `account_info` (`account_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_payment_targetBankBranch` FOREIGN KEY (`target_bank_branch_id`) REFERENCES `bank_branch` (`bank_branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_payment_teller` FOREIGN KEY (`teller`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `internal_payment_transaction`
--

LOCK TABLES `internal_payment_transaction` WRITE;
/*!40000 ALTER TABLE `internal_payment_transaction` DISABLE KEYS */;
INSERT INTO `internal_payment_transaction` VALUES (17,'2015-07-24','Sang',100,'2015-07-24 10:46:22','2015-07-24 10:52:58','Approved','Transfer','2459874562214',1,'0381000334020',1,1,2),(18,'2015-07-24','Sang',100,'2015-07-24 10:46:45',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(19,'2015-07-24','Sang',100,'2015-07-24 10:46:57',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(20,'2015-07-24','Sang',100,'2015-07-24 10:47:12',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(21,'2015-07-24','Sang',100,'2015-07-24 10:47:18',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(22,'2015-07-24','Sang',100,'2015-07-24 10:47:24',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(23,'2015-07-24','Sang',100,'2015-07-24 10:47:28',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(24,'2015-07-24','Sang',100,'2015-07-24 10:47:33',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(25,'2015-07-24','Sang',100,'2015-07-24 10:47:37',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(26,'2015-07-24','Sang',100,'2015-07-24 10:47:45',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(27,'2015-07-24','Sang',100,'2015-07-24 10:47:53',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(28,'2015-07-24','Sang',100,'2015-07-24 10:47:57',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(29,'2015-07-24','Sang',100,'2015-07-24 10:48:02',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(30,'2015-07-24','Sang',100,'2015-07-24 10:48:12',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(31,'2015-07-24','Sang',100,'2015-07-24 10:48:15',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(32,'2015-07-24','Sang',100,'2015-07-24 10:48:19',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(33,'2015-07-24','Sang',100,'2015-07-24 10:48:22',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(34,'2015-07-24','Sang',100,'2015-07-24 10:48:26',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(35,'2015-07-24','Sang',100,'2015-07-24 10:48:29',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(36,'2015-07-24','Sang',100,'2015-07-24 10:48:57',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(37,'2015-07-24','Sang',100,'2015-07-24 10:49:01',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2),(38,'2015-07-24','Sang',100,'2015-07-24 10:49:07',NULL,'Pending','Transfer','2459874562214',1,'0381000334020',1,1,2);
/*!40000 ALTER TABLE `internal_payment_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Teller'),(2,'Supervisor');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_group`
--

DROP TABLE IF EXISTS `transaction_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_group`
--

LOCK TABLES `transaction_group` WRITE;
/*!40000 ALTER TABLE `transaction_group` DISABLE KEYS */;
INSERT INTO `transaction_group` VALUES (1,'Group 1',2);
/*!40000 ALTER TABLE `transaction_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdrawal_transaction`
--

DROP TABLE IF EXISTS `withdrawal_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withdrawal_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `date` date DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `detail` varchar(45) DEFAULT NULL,
  `account` varchar(15) NOT NULL,
  `bank_branch_id` int(11) NOT NULL,
  `teller` int(11) NOT NULL,
  `supervisor` int(11) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `FK_withdraw_bankBranch_idx` (`bank_branch_id`),
  KEY `FK_withdraw_teller_idx` (`teller`),
  KEY `FK_withdraw_supervisor_idx` (`supervisor`),
  KEY `FK_withdraw_account_idx` (`account`),
  CONSTRAINT `FK_withdraw_account` FOREIGN KEY (`account`) REFERENCES `account_info` (`account_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_withdraw_bankBranch` FOREIGN KEY (`bank_branch_id`) REFERENCES `bank_branch` (`bank_branch_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_withdraw_supervisor` FOREIGN KEY (`supervisor`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_withdraw_teller` FOREIGN KEY (`teller`) REFERENCES `employee` (`employee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdrawal_transaction`
--

LOCK TABLES `withdrawal_transaction` WRITE;
/*!40000 ALTER TABLE `withdrawal_transaction` DISABLE KEYS */;
INSERT INTO `withdrawal_transaction` VALUES (51,100,'2015-07-24 10:45:14','2015-07-24 10:54:57','2015-07-24','Approved','Withdraw','1902834906201',1,1,2),(52,100,'2015-07-24 10:45:16',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(53,100,'2015-07-24 10:45:17',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(54,100,'2015-07-24 10:45:18',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(55,100,'2015-07-24 10:45:19',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(56,100,'2015-07-24 10:45:20',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(57,100,'2015-07-24 10:45:21',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(58,100,'2015-07-24 10:45:22',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(59,100,'2015-07-24 10:45:23',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(60,100,'2015-07-24 10:45:24',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(61,100,'2015-07-24 10:45:25',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(62,100,'2015-07-24 10:45:26',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(63,100,'2015-07-24 10:45:27',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(64,100,'2015-07-24 10:45:28',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(65,100,'2015-07-24 10:45:29',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(66,100,'2015-07-24 10:45:30',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(67,100,'2015-07-24 10:45:31',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(68,100,'2015-07-24 10:45:32',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(69,100,'2015-07-24 10:45:33',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(70,100,'2015-07-24 10:45:34',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(71,100,'2015-07-24 10:45:35',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(72,100,'2015-07-24 10:45:36',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(73,100,'2015-07-24 10:45:37',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(74,100,'2015-07-24 10:45:38',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(75,100,'2015-07-24 10:45:40',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(76,100,'2015-07-24 10:45:41',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(77,100,'2015-07-24 10:45:42',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(78,100,'2015-07-24 10:45:43',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(79,100,'2015-07-24 10:45:44',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(80,100,'2015-07-24 10:45:45',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(81,100,'2015-07-24 10:45:46',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(82,100,'2015-07-24 10:45:47',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2),(83,100,'2015-07-24 10:45:48',NULL,'2015-07-24','Pending','Withdraw','1902834906201',1,1,2);
/*!40000 ALTER TABLE `withdrawal_transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-24 11:20:27
